<?php
// Front end scripts
add_action( 'wp_enqueue_scripts', 'pm_pdlp_enqueue_scripts' );
function pm_pdlp_enqueue_scripts() {
	$post_type = get_post_type( get_the_ID() );
	
	wp_register_script( 'pmillrebrander', PM_PDLP_DIR.'modules/js/front.js', array('jquery'), '', true );
	wp_register_script( 'pdlp-magnific', PM_PDLP_DIR.'modules/js/MagnificPopup-0.9.9.js', array('jquery'), '', true );
	wp_register_style( 'pmillrebrander', PM_PDLP_DIR.'modules/css/front.css', '', '1.0', 'all' );
	wp_register_style( 'pdlp-magnific', PM_PDLP_DIR.'modules/css/MagnificPopup-0.9.9.css', '', '1.0', 'all' );
	
	if( $post_type == 'pm_pdlp' ) {
		wp_enqueue_script( 'pmillrebrander' );
		wp_enqueue_script( 'pdlp-magnific' );
		wp_enqueue_style( 'pmillrebrander' );
		wp_enqueue_style( 'pdlp-magnific' );
		wp_localize_script( 'pmillrebrander', 'pdlp', array( 
			'ajaxurl' => admin_url( 'admin-ajax.php' ),
			'pdlpNonce' => wp_create_nonce( 'pmillrebrander' )
			) );
	}
}

// Backend scripts
add_action( 'admin_enqueue_scripts', 'pm_pdlp_admin_scripts' );
function pm_pdlp_admin_scripts( $hook ) {
	global $post;

	// Scripts
	wp_register_script( 'pdlp-admin', PM_PDLP_DIR.'modules/js/admin.js', array('jquery'), false, true );
	// Styles
	wp_register_style( 'pdlp-admin', PM_PDLP_DIR.'modules/css/admin.css', '', false, 'all' );
	wp_register_style( 'pdlp-fontello', PM_PDLP_DIR.'modules/css/fontello-e16d046a/css/dlpages.css', '', false, 'all' );

	if( $hook == 'post-new.php' || $hook == 'post.php' || $hook == 'edit.php' ) {
		if( $post->post_type === 'pm_pdlp' ) {
			wp_enqueue_script( 'pdlp-admin' );
			wp_enqueue_style( 'pdlp-admin' );
			wp_enqueue_style( 'pdlp-fontello' );
		}
	}

}
?>