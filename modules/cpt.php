<?php
function pm_pdlp_add_post_type() {
  $labels = array(
    'name' => _x('Download Pages', 'post type general name', 'pm-download-pages'),
    'singular_name' => _x('Download Page', 'post type singular name', 'pm-download-pages'),
    'add_new' => _x('Add New', 'page', 'pm-download-pages'),
    'add_new_item' => __('Add New Download Page', 'pm-download-pages'),
    'edit_item' => __('Edit Download Page', 'pm-download-pages'),
    'new_item' => __('New Download Page', 'pm-download-pages'),
    'all_items' => __('All Download Page', 'pm-download-pages'),
    'view_item' => __('View Download Page', 'pm-download-pages'),
    'search_items' => __('Search Download Page', 'pm-download-pages'),
    'not_found' =>  __('No Download Page found', 'pm-download-pages'),
    'not_found_in_trash' => __('No Download Page found in Trash', 'pm-download-pages'), 
    'parent_item_colon' => '',
    'menu_name' => __('Download Pages', 'pm-download-pages')

  );
  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true, 
    'show_in_menu' => true,
    'show_in_nav_menus' => true,
    'show_in_admin_bar' => true,
    'menu_position' => '7',
    'menu_icon' => 'dashicons-download',
    'query_var' => true,
    'rewrite' => array( 'slug' => 'downloads' ),
    'capability_type' => 'post',
    'hierarchical' => false,
    'has_archive' => false, 
    //'supports' => array( 'title', 'editor' ),
    'supports' => array( 'title' ),
    'register_meta_box_cb' => 'pm_pdlp_add_custom_box'
  ); 
  register_post_type('pm_pdlp', $args);
}
add_action( 'init', 'pm_pdlp_add_post_type' );

// Add columns for overview screen
add_filter( 'manage_pm_pdlp_posts_columns', 'pm_pdlp_modify_columns' );
function pm_pdlp_modify_columns( $cols ) {
    $cols = array(
        'cb' => '<input type="checkbox" />',
        'title' => __('Title', 'pm-download-pages'),
        'ready' => __('Ready to sell', 'pm-download-pages'),
        'crosssell' => __('Cross sell', 'pm-download-pages'),
        'access' => __('aMember access', 'pm-download-pages'),
        'date' => __('Date', 'pm-download-pages')
        );
    return $cols;
}

// Overview screen column data
add_action( 'manage_posts_custom_column', 'pm_pdlp_custom_columns', 10, 2 );
function pm_pdlp_custom_columns( $column, $post_id ) {
    switch ( $column ) {
        case 'ready':
            $ready = get_post_meta( $post_id, 'pm_pdlp_salestatus', true );
            if( $ready == '1' ) {
                echo "<i class='pdlp-ok'></i> Yes";
            } else {
                echo "<i class='pdlp-cancel'></i> No";
            }
            break;
        case 'crosssell':
            $crosssell = get_post_meta( $post_id, 'crosssell', true );
            if( empty( $crosssell ) ) {
                echo "<i class='pdlp-cancel stage-1'></i> No";
            } else {
                echo "<i class='pdlp-ok'></i> Yes";
            }
            break;
        case 'access':
            pm_pdlp_getAmbrProductList($post_id, 'pm_pdlp_pid');
            break;
    }
}

// Make columns sortable
add_filter( 'manage_edit-pm_pdlp_sortable_columns', 'pm_pdlp_sortable_columns' );
function pm_pdlp_sortable_columns() {
    return array(
        'ready' => 'ready',
        'crosssell' => 'crosssell',
        'access' => 'access'
        );
}

// Make columns filterable
//https://yoast.com/custom-post-type-snippets/
?>