<?php
// Display custom template for download pages
add_filter( 'template_include', 'pm_pdlp_customTemplate' );
function pm_pdlp_customTemplate( $template ) {
	global $wp_query;
	
	// Example for custom post type
	if( ( get_query_var('post_type') == 'pm_pdlp' ) || ( is_front_page() && is_singular('pm_pdlp') ) ) {
		$template = PM_PDLP_PATH .'/modules/templates/default.php';

	}

	return $template;
}

// Ajax actions
add_action( 'wp_ajax_pmillrebrander', 'pm_pdlp_action_function' );
add_action( 'wp_ajax_nopriv_pmillrebrander', 'pm_pdlp_action_function');
function pm_pdlp_action_function() {
	// Check nonce
	$nonce = $_POST['pdlpNonce'];
	if( !wp_verify_nonce( $nonce, 'pmillrebrander' ) )
		die('Busted!');

	$pm_pdlp_opts = get_option( 'pm_pdlp_options' );
	$post_id = ( is_numeric( $_POST['post_id'] ) ) ? sanitize_text_field( $_POST['post_id'] ) : '' ;
	$pluginFileName =  get_post_meta( $post_id, 'pm_pdlp_pluginfilename', true );
	$toolsFileName =  get_post_meta( $post_id, 'pm_pdlp_toolsfilename', true );
	$pu_override = ( empty( $_POST['pu_override'] ) ) ? 0 : 1 ;

	// Is personal use?
	if( empty( $pu_override ) ) {
		// Check if fields are filled in and are not the same as ours
		if( empty( $_POST['pluginName'] ) || empty( $_POST['pluginUri'] ) || empty( $_POST['pluginDesc'] ) || empty( $_POST['authorName'] ) || empty( $_POST['authorUri'] ) ){
				echo '<span class="error">All fields (except description) are required to be changed. Please check your submission and <a href="">try again</a>.</span>';
				exit();
		} elseif ($_POST['pluginName'] == $_POST['placeholderPluginName']) {
			echo '<span class="error">Plugin name cannot be '.stripslashes($_POST['placeholderPluginName']).'. Please enter a different name and <a href="">try again</a>.</span>';
			exit();
		} elseif ( stripos($_POST['pluginUri'], 'pluginmill') ) {
			echo '<span class="error">Plugin URL cannot be PluginMill.com. Please enter your own URL and <a href="">try again</a>.</span>';
			exit();
		} elseif ( stripos($_POST['authorName'], 'Lynette Chandler') !== false ) {
			echo '<span class="error">Plugin author cannot be Lynette Chandler. Please enter your own name and <a href="">try again</a>.</span>';
			exit();
		} elseif ( ( stripos($_POST['authorUri'], 'pluginmill') !==false ) || ( stripos($_POST['authorUri'], 'techbasedmarketing') !== false ) ) {
			echo '<span class="error">Author URL cannot be PluginMill.com or TechBasedMarketing.com. Please enter your own URL and <a href="">try again</a>.</span>';
			exit();
		} elseif ( ( $_POST['addUpdater'] == 1 ) && ( empty( $_POST['updateCheckUrl'] ) ) ) {
			echo '<span class="error">Update Checker URL cannot be empty. Please enter your own URL and <a href="">try again</a>.</span>';
			exit();
		}
	}

	// Validate/Sanitize Data
	$new_pluginName = sanitize_text_field( $_POST['pluginName'] );
	$new_pluginUri = esc_url_raw( $_POST['pluginUri'] );
	$new_pluginDesc = wp_kses_post( $_POST['pluginDesc'] );
	$new_authorName = sanitize_text_field( $_POST['authorName'] );
	$new_authorUri = esc_url_raw( $_POST['authorUri'] );
	$new_addUpdater = ( empty( $_POST['addUpdater'] ) ) ? '' : sanitize_key( $_POST['addUpdater'] ) ;
	$new_updateCheckUrl = ( empty( $_POST['updateCheckUrl'] ) ) ? '' : esc_url_raw( $_POST['updateCheckUrl'] );
	$new_updatedPluginUrl = ( empty( $_POST['updatedPluginUrl'] ) ) ? '' : esc_url_raw( $_POST['updatedPluginUrl'] );

	// Build Amazon signed URL
	$signedURL = new pmill_getAzSignedUrl($pm_pdlp_opts['defbucket'], $pm_pdlp_opts['bucketfolder'], $pluginFileName, $pm_pdlp_opts['defexpiry'], $pm_pdlp_opts['amzaccess'], $pm_pdlp_opts['amzsecret']);

	// Perform rebranding
	pm_pdlp_brandit( $signedURL->signedUrl(), $pluginFileName, $new_pluginName, $new_pluginUri, $new_pluginDesc, $new_authorName, $new_authorUri, $new_addUpdater, $new_updateCheckUrl, $new_updatedPluginUrl, $pu_override );

	// Generate response
	/*
	$r = array (
		'post_id' => $post_id,
		'signedURL' => $signedURL->signedUrl(),
		'pluginFileName' => $pluginFileName,
		'pluginName' => $new_pluginName
	);
	$response = json_encode( $r );

	// response output
	echo $response;
	*/
	
	exit();
}

/* * * * * * * * * * * * *
 * Copy and brand plugin
 * * * * * * * * * * * * */

// Class to get Amazon Signed URL
class pmill_getAzSignedUrl {
	// Properties
	private $bucket;
	private $folder;
	private $file;
	private $expiry;
	private $access_key;
	private $secret;

	// Assigning values
	public function __construct( $bucket, $folder, $file, $expiry, $access_key, $secret ) {
		$this->bucket = stripslashes( $bucket );
		$this->file = stripslashes( $file );
		$this->folder = stripslashes( $folder );
		$this->expiry = (is_numeric( $expiry ) ) ? $expiry : '0' ;
		$this->access_key = stripslashes( $access_key );
		$this->secret = stripslashes( $secret );
	}

	// Create method (function)
	public function signedUrl() {
		// Expiry time
		$expiry = time()+$this->expiry*60;
		// Sign the URL string
		$string_to_sign = "GET\n\n\n$expiry\n/".str_replace(".s3.amazonaws.com","",$this->bucket)."/$this->file";
	    $signature = urlencode(base64_encode((hash_hmac("sha1", utf8_encode($string_to_sign), $this->secret, TRUE))));

		return 'http://'.$this->bucket.'.s3.amazonaws.com/'.$this->file.'?AWSAccessKeyId='.$this->access_key.'&Expires='.$expiry.'&Signature='.$signature;
	}
}

// Function to do actual branding
/* Troubleshooting: Check $fileName, $versionNumber. $fileName in particular can mess up
 * entire re-branded pack
 *
 */
function pm_pdlp_brandit( $sourcePlugin, $pluginFileName, $PluginName, $PluginUri, $PluginDesc, $PluginAuthor, $AuthorUri, $addUpdater, $updateCheckUrl, $updatedPluginUrl, $pu_override ) {
	$tmpDirName = uniqid();
	//$fileName = preg_replace('/-\d\.\d{1,2}\.\d{1,2}/', '', $pluginFileName);
	$searchFileString = array('/^privatelabel\//', '/-\d+\.\d+\.\d+\.\d+\.zip/');
	$replaceFileString = array('', '');
	$fileName = preg_replace($searchFileString, $replaceFileString, $pluginFileName);
	//$versionNumber = substr($pluginFileName, -11);
	$versionNumber = preg_replace('/\w+\/(\w+-)+/', '', $pluginFileName);
	$versionDigits = preg_replace( '/\.zip/', '', $versionNumber );
	$newPluginFileName = preg_replace('/\s+/', '-', $PluginName);
	$trimmedUpdateCheckUrl = rtrim($updateCheckUrl, '/');
	$updateCheckUrl = ( $pu_override === 1 ) ? $updateCheckUrl : $trimmedUpdateCheckUrl.'/'.$tmpDirName.'-'.$newPluginFileName.'-'.$versionDigits.'.json' ;

	$copiedPlugin = PM_PDLP_PATH.'/modules/deliver/'.$tmpDirName.'-'.$newPluginFileName.'-'.$versionNumber;

	//echo "pluginFilename: ".$pluginFileName."<br>Filename: ".$fileName."<br>version:".$versionNumber." version digits:".$versionDigits; exit();

	if( copy($sourcePlugin, $copiedPlugin) ) {
		//echo '<p>Plugin Copied</p>';
	} else {
		echo '<p>Could not copy plugin</p>';
		//echo '<p>Source: '.$sourcePlugin.'</p>';
	}

	// Create object
	$zip = new ZipArchive();
	$pluginIndex = $fileName.'/'.$fileName.'.php';

	//Open archive
	if ( $zip->open($copiedPlugin) === TRUE ) {
		//echo '<p>Getting ready to rebrand</p>';
		// Get file from zip
		$findIndex = $zip->getFromName( $pluginIndex );

		// $addUpdater should come from form, passed into function
		if( $addUpdater == 1 ) {

			// Build update check code
			$update_var = mb_strtolower( preg_replace('/\s+/', '_', $PluginName), 'UTF-8' );
			$updateString = "";
			$updateString .= "require_once('plugin-updates/plugin-update-checker.php');\n";
			$updateString .= "\$pm_".$update_var."_update_check = PucFactory::buildUpdateChecker('".$updateCheckUrl."', __FILE__);";

			// Build merge arrays
			$mergecodes = array(
				'__PluginName__',
				'__PluginUri__',
				'__PluginDesc__',
				'__PluginAuthor__',
				'__AuthorUri__',
				'##addUpdateCheck##'
				);
			$brandinfo = array(
				$PluginName,
				$PluginUri,
				$PluginDesc,
				$PluginAuthor,
				$AuthorUri,
				$updateString
				);

			if( empty( $pu_override ) ) {
				// Get rid of trailing slash if any
				$trimmedUpdatedPluginUrl = rtrim($updatedPluginUrl, '/');
				// Generate JSON file
				$get_json_file_url = pm_pdlp_build_update_check_json( $fileName, $PluginName, $versionNumber, $PluginDesc, $PluginAuthor, $trimmedUpdatedPluginUrl.'/'.$tmpDirName.'-'.$newPluginFileName.'-'.$versionNumber, $tmpDirName );
			}

		} else {
			$mergecodes = array(
				'__PluginName__',
				'__PluginUri__',
				'__PluginDesc__',
				'__PluginAuthor__',
				'__AuthorUri__',
				'##addUpdateCheck##'
				);
			$brandinfo = array(
				$PluginName,
				$PluginUri,
				$PluginDesc,
				$PluginAuthor,
				$AuthorUri,
				''
				);
		}

		// Modify file
		$newFile = str_replace( $mergecodes, $brandinfo, $findIndex );
		// Delete original file
		$zip->deleteName( $pluginIndex );
		// Replace with new file created
		$zip->addFromString( $pluginIndex, $newFile );
		// Close
		$zip->close();

		echo '<p>All done! Click the button(s) below to begin downloading.</p><p class="center"><a href="'.PM_PDLP_DIR.'modules/deliver/'.$tmpDirName.'-'.$newPluginFileName.'-'.$versionNumber.'" class="pmill-btn download">Download Plugin</a></p>';
		if( $addUpdater == '1' ) {
			if( empty( $get_json_file_url ) ) {
				if( empty( $pu_override ) ) {
					echo '<p>Oops! There was an error generating your update checker file. Please copy and paste this error notice to send to support.</p>';
				}
				} else {
				echo '<p class="center"><a href="'.$get_json_file_url.'" class="pmill-btn download">Download Update Checker File</a></p><p class="center">(Use right click, Save As)</p>';
			}
		}

	} else {
		die( 'Could not open plugin archive.' );
	}
}

// Add upload button
add_action( 'media_buttons','pm_pdlp_s3upload',100);
function pm_pdlp_s3upload(){
    global $pagenow,$typenow;

    if (!in_array( $pagenow, array( 'post.php', 'post-new.php' ) ))
        return;
    if( $typenow != 'pm_pdlp' )
    	return;

    echo '<a href="'.PM_PDLP_DIR.'modules/libs/s3-upload-dialog.php?TB_iframe=true&height=155&width=300&inlineId=s3upload" title="Upload to S3" class="thickbox button"><span class="dashicons dashicons-upload"></span>Upload To S3</a>';
}

// Flatten array
function pm_pdlp_flatten($array) {
	$result = '';
    foreach ($array as $key => $value) {
    	$result = $value.",";
    }
    return $result;
}

// Create Update checker JSON file
function pm_pdlp_build_update_check_json( $fileName, $PluginName, $PluginVersion, $PluginDesc, $PluginAuthor, $updatedPluginUrl, $uniqueid ) {
	$modified_PluginName = preg_replace( '/\s+/', '-', $PluginName );
	$versionNumber = preg_replace( '/\.zip/', '', $PluginVersion );
	$json_string = '{
	    "name" : "'.$PluginName.'",
	    "slug" : "'.$fileName.'",
	    "version" : "'.$versionNumber.'",
	    "author" : "'.$PluginAuthor.'",
	    "download_url" : "'.$updatedPluginUrl.'",
	    "sections" : {
	        "description" : "'.$PluginDesc.'"
	    }
	}';

	$file = PM_PDLP_PATH.'/modules/deliver/'.$uniqueid.'-'.$modified_PluginName.'-'.$versionNumber.'.json';

	$json_file_created = file_put_contents($file, $json_string);
	if( $json_file_created ) {
		$json_file_url = PM_PDLP_DIR.'modules/deliver/'.$uniqueid.'-'.$modified_PluginName.'-'.$versionNumber.'.json';
		return $json_file_url;
	} else {
		return '0';
	}
}