<?php 
//Include SDK
if( !class_exists('Symfony\Component\ClassLoader\UniversalClassLoader') ) {
	require_once 'libs/aws-sdk/aws.phar';
}

if( !class_exists( 'pmAwsTasks' ) ) {
// Amazon Class
class pmAwsTasks {
	// Create some properties (variables)
	private $option;
	private $buckets = null;
	private $s3 = null;

	// Assign values
	public function __construct($option, $buckets, $s3) {
		$this->optionName = $option;
		$this->buckets = $buckets;
		$this->s3 = $s3;
		$this->optionVars = get_option($option);
	}

	// Test method
	public function Test() {
		echo "Option name: ".$this->optionName."<br>";
		print_r( $this->buckets );
	}

	// Get buckets
	public function pm_pdlp_listbuckets( $selectname ) {
		$buckets = $this->buckets;
		$pm_pdlp_opts = $this->optionVars;
		$defbucket = ( empty( $pm_pdlp_opts['defbucket'] ) ) ? '' : $pm_pdlp_opts['defbucket'] ;
	?>
		<select id="listbuckets" name="<?php echo $selectname; ?>">
			<option value="0">None</option>
			<?php
			foreach ($buckets as $bucket) {
			?>
			<option value="<?php echo $bucket['Name']; ?>" <?php selected( $defbucket, $bucket['Name'], true ); ?>><?php echo $bucket['Name']; ?></option>
			<?php
		}
		?>
		</select>
		<?php
	}

	// List files and folders
	public function pm_pdlp_getfiles($bucket, $element, $postID, $fieldname) {
		/*
		# http://stackoverflow.com/questions/21138673/how-to-get-commonprefixes-from-an-amazon-s3-listobjects-iterator
		# http://docs.aws.amazon.com/AmazonS3/latest/dev/ListingKeysHierarchy.html
		*/
		$s3 = $this->s3;

		$iterator = $s3->getIterator(
			'ListObjects',
			array(
				'Bucket' => $bucket,
				)
			/*array(
				'return_prefixes' => true,
				)*/
			);
		
		foreach ($iterator as $object) {
			if( $element == 'radio' ) {
				echo "<li><label><input type='radio' class='file' name='file' value='".basename($object['Key'])."' />".basename($object['Key'])."</label></li>";
			}
			if( $element == 'checkbox' ) {
				echo "<li><label><input type='checkbox' class='file' name='file' value='".basename($object['Key'])."' />".basename($object['Key'])."</label></li>";
			}
			if( $element == 'select' ) {
				echo "<option value='".basename($object['Key'], '.zip')."' ".selected( get_post_meta( $postID, $fieldname, true ), basename($object['Key'], '.zip'), true ).">".basename($object['Key'])."</option>";
			}
		}
	}

	// List folders only
	public function pm_pdlp_getfolders($bucket, $element, $fieldname) {
		$pm_pdlp_opts = $this->optionVars;
		$s3 = $this->s3;
		$bucketfolder = ( empty( $pm_pdlp_opts['bucketfolder'] ) ) ? '' : stripslashes( $pm_pdlp_opts['bucketfolder'] );

		if( !empty( $s3 ) ) {
			$iterator = $s3->getIterator(
				'ListObjects',
				array(
					'Bucket' => $bucket,
					'Delimiter' => '/'
					),
				array(
					'return_prefixes' => true,
					)
				);
			$objects = $iterator->toArray();

			foreach ($objects as $object) {
				if( $element == 'radio' ) {
					echo "<li><label><input type='radio' class='file' name='file' value='".basename($object['Key'])."' />".basename($object['Key'])."</label></li>";
				}
				if( $element == 'checkbox' ) {
					echo "<li><label><input type='checkbox' class='file' name='file' value='".basename($object['Key'])."' />".basename($object['Key'])."</label></li>";
				}
				if( $element == 'select' ) {
					$foldername = str_replace( '/', '', $object['Prefix'] );
					echo '<option value="'.$foldername.'" '.selected( $bucketfolder, $foldername, true ).'>'.$foldername.'</option>';
				}
			}
		}
	}

	// Upload method (untested Sep 12, 2014)
	public function Upload() {
		$option = $this->optionVars;
		$form['policy'] = '{
		    "expiration": "'.$option['defexpiry'].'",
		        "conditions": [
		            {
		                "acl": "private"
		            },
		            {
		                "bucket": "'.$option['defbucket'].'"
		            },
		            {
		            	"success_action_status": "201"
		            },
		            [
		                "starts-with",
		                "$key",
		                "plugins/"
			        ]
		        ]
		    }';
	    $form['policy_encoded'] = base64_encode($form['policy']);
	    $form['signature'] = base64_encode(hash_hmac('sha1', $form['policy_encoded'], $form['aws_secret_key'], true));

		$go = '<form>here</form>';
		return $go;

		return <<<HTML
	    <form class="awsUpload" action="http://{$option['defbucket']}.s3.amazonaws.com" method="POST" enctype="multipart/form-data">
    	<input type="hidden" name="key" value="{$option['bucketfolder']}/${filename}">
    	<input type="file" name="file">
    	<input type="submit" value="Upload to Amazon S3">
	    </form>
HTML;
	}
}
}
?>