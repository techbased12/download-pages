<?php 
/*
 * Template to use for Download Pages
 */
include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
//include_once('/home/plugawes/public_html/account/library/Am/Lite.php');
include_once('/Users/lynette/Documents/Websites/www.pluginmill.dev/account/library/Am/Lite.php');

//* Force full width content layout
add_filter( 'genesis_site_layout', '__genesis_return_full_width_content' );
//* Remove the entry meta in the entry header (requires HTML5 theme support)
remove_action( 'genesis_entry_header', 'genesis_post_info', 12 );
//* Remove the entry meta in the entry footer (requires HTML5 theme support)
remove_action( 'genesis_entry_footer', 'genesis_post_meta' );
//* Remove genesis_entry_content
remove_action( 'genesis_entry_content', 'genesis_do_post_content' );

add_action( 'genesis_entry_content', 'pm_pdlp_post_content' );
function pm_pdlp_post_content() {
	global $post;
	$meta = get_post_meta( $post->ID );
	$pm_pdlp_opts = get_option( 'pm_pdlp_options' );
	$amember = Am_Lite::getInstance();
	$user = $amember->getUser();
	$username = ( empty($user['name_f']) ) ? 'there' : $user['name_f'] ;
	$browser = $_SERVER['HTTP_USER_AGENT'];

	// Display global thanks message
	if( !empty( $pm_pdlp_opts['thanks'] ) ) {
		$mergecodes = array( '%%name%%', '%%product_name%%' );
		$mergevalues = array( $username, get_the_title( $post->ID ) );
		$newmsg = str_replace($mergecodes, $mergevalues, $pm_pdlp_opts['thanks']);
		echo wpautop( $newmsg );
	}
	
	// Display the content
	the_content();

	// Display downloads
	// If user has plr access to this product
	$reqMembership = get_post_meta( $post->ID, 'pm_pdlp_pid', true );
	$hasplr = $amember->haveSubscriptions( $reqMembership );
	// If user has personal use access to this product
	$reqPuMembership = get_post_meta( $post->ID, 'pm_pdlp_personaluse_pid', true );
	$haspu = $amember->haveSubscriptions( $reqPuMembership );
	/*
	print_r($reqMembership);
	echo "Required: $reqMembership<br>Do we have access?";
	print_r($amember->haveSubscriptions( $reqMembership ));
	echo "<br>";
	echo "plr: $hasplr, personal: $haspu";*/
	
	if( !empty($hasplr) ) {
		// Have private label license
		// Display step 1 - download selling tools
		$pluginInfo = get_post_meta( $post->ID, 'pm_pdlp_plugin_info', true );
		$toolsFileName = get_post_meta( $post->ID, 'pm_pdlp_toolsfilename', true );
		$sellingToolsURL =  new pmill_getAzSignedUrl($pm_pdlp_opts['defbucket'], $pm_pdlp_opts['bucketfolder'], $toolsFileName, $pm_pdlp_opts['defexpiry'], $pm_pdlp_opts['amzaccess'], $pm_pdlp_opts['amzsecret']);
		$pluginName = ( empty( $pluginInfo['def_name'] ) ) ? $post->post_title : esc_attr( $pluginInfo['def_name'] );
		$plugin_def_desc = ( empty($pluginInfo['def_desc'] ) ) ? '' : esc_textarea( $pluginInfo['def_desc'] ) ;
		// Get and use default user values if any
		$pluginUri = ( empty( $user['pluginuri'] ) ) ? '' : stripslashes( $user['pluginuri'] ) ;
		$authorName = ( empty( $user['pluginauthorname'] ) ) ? '' : stripslashes( $user['pluginauthorname'] ) ;
		$authorUri = ( empty( $user['pluginauthoruri'] ) ) ? '' : stripslashes( $user['pluginauthoruri'] ) ;
		$updateReady = ( $pluginInfo['def_update_ready'] === 1 ) ? 1 : 0 ;
		?>
		<h3>Step 1: Download Your Selling Tools</h3>
		<p class="center">
			<a href="<?php echo $sellingToolsURL->signedUrl(); ?>" class="pmill-btn download">Download Selling Tools</a>
		</p>
		<?php
		// Display step 2 - rebrand and download plugin		
		?>
		<h3>Step 2: Brand & Download Your Plugin</h3>
		<?php 
		if( preg_match('/MSIE ([0-9].[0-9]{1,2})/', $browser)) {
			?>
			<div class="error">Older versions of Internet Explorer do not play nice with the brander. If you're unable to switch to a different browser, click Brand and Download to obtain the plugin and re-brand manually. Manual re-branding instructions are found in the sales tools package.</div>
			<?php
		}
		?>
		<div id="rebrandProcess">
			<img src="<?php echo PM_PDLP_DIR; ?>modules/images/branding.gif" alt="Rebranding in process"><br>
			Please be patient. This might take a few minutes.
		</div>
		<div id="rebrandResponse"></div>
		<form id="pmillrebrander" action="" method="POST">
			<label for="plugin-name">Plugin name: </label>
			<input type="text" id="plugin-name" class="rebrandField" name="pluginName" placeholder="<?php echo $pluginName; ?>">
			<label for="plugin-uri">Plugin URL: </label>
			<input type="text" id="plugin-uri" class="rebrandField" name="pluginUri" placeholder="http://PluginMill.com" value="<?php echo $pluginUri; ?>">
			<label for="plugin-desc">Plugin description</label>
			<textarea name="pluginDesc" id="plugin-desc" cols="30" rows="10" placeholder="<?php echo $plugin_def_desc; ?>"></textarea>
			<label for="author-name">Author name: </label>
			<input type="text" id="author-name" class="rebrandField" name="authorName" placeholder="Lynette Chandler" value="<?php echo $authorName; ?>">
			<label for="author-uri">Author URL: </label>
			<input type="text" id="author-uri" class="rebrandField" name="authorUri" placeholder="http://TechBasedMarketing.com" value="<?php echo $authorUri; ?>">
			<?php if( $updateReady ) { ?>
				<div id="add-update-option"><label for="add-updater">Add Automatic Updater</label>
				<input type="checkbox" id="add-updater" class="rebrandField" name="addUpdater"></div>
				<div id="warning-update-check-url">You have selected to add the updater. Please enter the URL to the update check file (.json) and the URL to the plugin, without file name.</div>
				<label for="update-check-url">Automatic Update Check URL</label>
				<input type="text" id="update-check-url" class="rebrandField" name="updateCheckUrl">
				<label for="updated-plugin-url">Automatic Update Plugin URL</label>
				<input type="text" id="updated-plugin-url" class="rebrandField" name="updatedPluginUrl">
			<?php } else { ?>
				<input type="hidden" id="add-updater" class="rebrandField" name="addUpdater" value="0">
			<?php } ?>

			<input type="hidden" name="post_id" value="<?php echo $post->ID; ?>">
			<p class="center"><input type="submit" id="rebranderSubmit" class="pmill-btn download" value="Brand & Download"></p>
		</form>
		<?php
	} elseif ( !empty( $haspu ) ) {
		// User has personal use license
		$pluginInfo = get_post_meta( $post->ID, 'pm_pdlp_plugin_info', true );
		$puGuideFileName = get_post_meta( $post->ID, 'pm_pdlp_pu_guide_filename', true );
		$puGuideURL =  new pmill_getAzSignedUrl($pm_pdlp_opts['defbucket'], $pm_pdlp_opts['bucketfolder'], $puGuideFileName, $pm_pdlp_opts['defexpiry'], $pm_pdlp_opts['amzaccess'], $pm_pdlp_opts['amzsecret']);
		$pluginName = ( empty( $pluginInfo['def_name'] ) ) ? $post->post_title : esc_attr( $pluginInfo['def_name'] );
		$plugin_def_desc = ( empty($pluginInfo['def_desc'] ) ) ? '' : esc_textarea( $pluginInfo['def_desc'] ) ;
		$pm_pdlp_pu_updater = get_post_meta( $post->ID, 'pm_pdlp_pu_updater', true );
		$updateCheckUrl = ( empty( $pm_pdlp_pu_updater['update_check_url'] ) ) ? '' : $pm_pdlp_pu_updater['update_check_url'] ;
		$updatedPluginUrl = ( empty( $pm_pdlp_pu_updater['plugin_update_url'] ) ) ? '' : $pm_pdlp_pu_updater['plugin_update_url'] ;
		?>
		<h3>Step 1: Download Your User Guide</h3>
		<p class="center">
			<a href="<?php echo $puGuideURL->signedUrl(); ?>" class="pmill-btn download">Download User Guide</a>
		</p>
		<h3>Step 2: Download Your Plugin</h3>
		<div id="rebrandProcess">
			<img src="<?php echo PM_PDLP_DIR; ?>modules/images/branding.gif" alt="Rebranding in process"><br>
			Please be patient. This might take a few minutes.
		</div>
		<div id="rebrandResponse"></div>
		<form id="pmillrebrander" action="" method="POST">
			<input type="hidden" id="plugin-name" class="rebrandField" name="pluginName" value="<?php echo $pluginName; ?>">
			<input type="hidden" id="plugin-uri" class="rebrandField" name="pluginUri" value="http://PluginMill.com">
			<textarea name="pluginDesc" id="plugin-desc" style="display: none;"><?php echo $plugin_def_desc; ?></textarea>
			<input type="hidden" id="author-name" class="rebrandField" name="authorName" value="Lynette Chandler">
			<input type="hidden" id="author-uri" class="rebrandField" name="authorUri" value="http://TechBasedMarketing.com">
			<input type="hidden" id="add-updater" class="rebrandField" name="addUpdater" value="1">
			<input type="hidden" id="update-check-url" class="rebrandField" name="updateCheckUrl" value="<?php echo $updateCheckUrl; ?>">
			<input type="hidden" id="updated-plugin-url" class="rebrandField" name="updatedPluginUrl" value="<?php echo $updatedPluginUrl; ?>">
			<input type="hidden" name="pu_override" value="1">
			<input type="hidden" name="post_id" value="<?php echo $post->ID; ?>">
			<p class="center"><input type="submit" class="pmill-btn download" id="rebranderSubmit" value="Compile Plugin"></p>
		</form>
		<?php
	} else {
		// User has neither license
		$permalink = urlencode(get_permalink($post->ID));
		?>
		<p class="center"><a href="#" data-mfp-src="#pdlp-noaccess" class="pmill-btn download noaccess">Click To Download</a></p>
		<div id="pdlp-noaccess" class="white-popup mfp-hide">
			<h3>Oops! Looks Like There's A Problem</h3>
			<p>To download <?php echo $post->post_title; ?> please <a href="http://<?php echo $pm_pdlp_opts['ambrdomain'].'/'.$pm_pdlp_opts['ambrfolder'].'/login/index?amember_redirect_url='.$permalink; ?>">login</a> first.</p>
		</div>
		<?php
	}
	
	if( !empty( $pm_pdlp_opts['crosssell'] ) || !empty( $meta['crosssell'][0] ) ) {
		?>
		<div id="cross-sell">
			<?php
			if( !empty( $meta['crosssell'][0] ) ) {
				echo wpautop( stripslashes( $meta['crosssell'][0] ), true );
			} else {
				echo wpautop( stripslashes( $pm_pdlp_opts['crosssell'] ), true );
			}
			?>
		</div>
		<?php
	}
	if( !empty( $pm_pdlp_opts['changelog'] ) || !empty( $meta['changelog'][0] ) ) {
		?>
		<div id="changelog">
			<h3>Change Log</h3>
			<?php
			if( !empty( $meta['changelog'][0] ) ) {
				echo wpautop( stripslashes( $meta['changelog'][0] ), true );
			} else {
				echo wpautop( stripslashes( $pm_pdlp_opts['changelog'] ), true );
			}
			?>
		</div>
		<?php
	}
}

genesis();