<?php 
/*
 * Template to use for Download Pages
 */
include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
include_once('/Users/lynette/Documents/Websites/www.pluginmill.dev/account/library/Am/Lite.php');

//* Force full width content layout
add_filter( 'genesis_site_layout', '__genesis_return_full_width_content' );
//* Remove the entry meta in the entry header (requires HTML5 theme support)
remove_action( 'genesis_entry_header', 'genesis_post_info', 12 );
//* Remove the entry meta in the entry footer (requires HTML5 theme support)
remove_action( 'genesis_entry_footer', 'genesis_post_meta' );
//* Remove genesis_entry_content
remove_action( 'genesis_entry_content', 'genesis_do_post_content' );

add_action( 'genesis_entry_content', 'pm_pdlp_post_content' );
function pm_pdlp_post_content() {
	global $post;
	$meta = get_post_meta( $post->ID );
	$pm_pdlp_opts = get_option( 'pm_pdlp_options' );
	$amember = Am_Lite::getInstance();
	$user = $amember->getUser();
	$username = ( empty($user['name_f']) ) ? 'there' : $user['name_f'] ;
	$browser = $_SERVER['HTTP_USER_AGENT'];

	// Display global thanks message
	if( !empty( $pm_pdlp_opts['thanks'] ) ) {
		$mergecodes = array( '%%name%%', '%%product_name%%' );
		$mergevalues = array( $username, get_the_title( $post->ID ) );
		$newmsg = str_replace($mergecodes, $mergevalues, $pm_pdlp_opts['thanks']);
		echo wpautop( $newmsg );
	}
	
	// Display the content
	the_content();

	// Display downloads
	// If user has no access to this product
	$reqMembership = get_post_meta( $post->ID, 'pm_pdlp_pid', true );
	/*
	echo "Required: $reqMembership<br>Do we have access?";
	print_r($amember->haveSubscriptions( $reqMembership ));
	echo "<br>";
	*/
	if( $amember->haveSubscriptions( $reqMembership ) == null ) {
		$permalink = urlencode(get_permalink($post->ID));
		?>
		<p class="center"><a href="#" data-mfp-src="#pdlp-noaccess" class="pmill-btn download noaccess">Click To Download</a></p>
		<div id="pdlp-noaccess" class="white-popup mfp-hide">
			<h3>Oops! Looks Like There's A Problem</h3>
			<p>To download <?php echo $post->post_title; ?> please <a href="http://<?php echo $pm_pdlp_opts['ambrdomain'].'/'.$pm_pdlp_opts['ambrfolder'].'/login/index?amember_redirect_url='.$permalink; ?>">login</a> first.</p>
		</div>
		<?php
	} else {
		// Display step 1 - download selling tools
		$pluginInfo = get_post_meta( $post->ID, 'pm_pdlp_plugin_info', true );
		$toolsFileName = get_post_meta( $post->ID, 'pm_pdlp_toolsfilename', true );
		$sellingToolsURL =  new pmill_getAzSignedUrl($pm_pdlp_opts['defbucket'], $pm_pdlp_opts['bucketfolder'], $toolsFileName.'.zip', $pm_pdlp_opts['defexpiry'], $pm_pdlp_opts['amzaccess'], $pm_pdlp_opts['amzsecret']);
		$pluginName = ( empty( $pluginInfo['def_name'] ) ) ? $post->post_title : esc_attr( $pluginInfo['def_name'] );
		$plugin_def_desc = ( empty($pluginInfo['def_desc'] ) ) ? '' : esc_textarea( $pluginInfo['def_desc'] ) ;
		?>
		<h3>Step 1: Download Your Selling Tools</h3>
		<p class="center">
			<a href="<?php echo $sellingToolsURL->signedUrl(); ?>" class="pmill-btn download">Download Selling Tools</a>
		</p>
		<?php
		// Display step 2 - rebrand and download plugin		
		?>
		<h3>Step 2: Brand & Download Your Plugin</h3>
		<?php 
		if( preg_match('/MSIE ([0-9].[0-9]{1,2})/', $browser)) {
			?>
			<div class="error">You are using an older version of Internet Explorer which may give unexpected results when branding your plugin. Please switch to Chrome, Firefox or use Internet Explorer 10 and up.</div>
			<?php
		}
		?>
		<div id="rebrandProcess">
			<img src="<?php echo PM_PDLP_DIR; ?>modules/images/branding.gif" alt="Rebranding in process"><br>
			Please be patient. This might take a few minutes.
		</div>
		<div id="rebrandResponse"></div>
		<form id="pmillrebrander" action="" method="POST">
			<label for="plugin-name">Plugin name: </label>
			<input type="text" id="plugin-name" class="rebrandField" name="pluginName" placeholder="<?php echo $pluginName; ?>">
			<label for="plugin-uri">Plugin URL: </label>
			<input type="text" id="plugin-uri" class="rebrandField" name="pluginUri" placeholder="http://PluginMill.com">
			<label for="plugin-desc">Plugin description</label>
			<textarea name="pluginDesc" id="plugin-desc" cols="30" rows="10" placeholder="<?php echo $plugin_def_desc; ?>"></textarea>
			<label for="author-name">Author name: </label>
			<input type="text" id="author-name" class="rebrandField" name="authorName" placeholder="Lynette Chandler">
			<label for="author-uri">Author URL: </label>
			<input type="text" id="author-uri" class="rebrandField" name="authorUri" placeholder="http://TechBasedMarketing.com">
			<input type="hidden" name="post_id" value="<?php echo $post->ID; ?>">
			<input type="submit" id="rebranderSubmit" value="Brand & Download">
		</form>
		<?php
	}
	
	if( !empty( $pm_pdlp_opts['crosssell'] ) || !empty( $meta['crosssell'][0] ) ) {
		?>
		<div id="cross-sell">
			<?php
			if( !empty( $meta['crosssell'][0] ) ) {
				echo wpautop( stripslashes( $meta['crosssell'][0] ), true );
			} else {
				echo wpautop( stripslashes( $pm_pdlp_opts['crosssell'] ), true );
			}
			?>
		</div>
		<?php
	}
}

genesis();
?>