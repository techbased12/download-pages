<?php 
add_action( 'add_meta_boxes', 'pm_pdlp_add_custom_box' ); // Called in register_post_type
function pm_pdlp_add_custom_box() {
  if( class_exists('Symfony\Component\ClassLoader\UniversalClassLoader') ) {
    // Get default options
    $pm_pdlp_opts = get_option('pm_pdlp_options');

    // Filter and sort out options
    $access_key = sanitize_text_field( $pm_pdlp_opts['amzaccess'] );
    $secret = sanitize_text_field( $pm_pdlp_opts['amzsecret'] );

    if( !empty($access_key) || !empty( $secret ) ) {
      $s3Client = Aws\Common\Aws::factory(array(
        'key' => $access_key,
        'secret' => $secret,
        ));
      $s3 = $s3Client->get('s3');
      // Get buckets & files
      $allbuckets = $s3->listBuckets()->toArray();
      $buckets = $allbuckets['Buckets'];
    }
  
    $args = array( $pm_pdlp_opts, $buckets, $s3 );
  }
  global $post;

	// Load metabox for specified post type only
    add_meta_box(
      'pm_pdlp_info',
      __( 'Plugin Information', 'pm-download-pages' ),
      'pm_pdlp_info_mbox',
      'pm_pdlp', 'normal', 'default',
      $post
      );
    add_meta_box(
      'pm_pdlp_delivery',
      __( 'Product Files & Access', 'pm-download-pages' ),
      'pm_pdlp_delivery_mbox',
      'pm_pdlp', 'normal', 'default',
      $args
      );
    add_meta_box(
      'pm_pdlp_salestatus',
      __( 'Sales Status', 'pm-download-pages' ),
      'pm_pdlp_salestatus_mbox',
      'pm_pdlp', 'side', 'default',
      $args
      );
}

// Metabox callback
function pm_pdlp_info_mbox( $post ) {
  wp_nonce_field( plugin_basename( __FILE__ ), 'pm_pdlp_nonce' );
  $pluginInfo = get_post_meta( $post->ID, 'pm_pdlp_plugin_info', true );
  $plugin_def_name = ( empty($pluginInfo['def_name'] ) ) ? '' : esc_attr( $pluginInfo['def_name'] ) ;
  $plugin_def_desc = ( empty($pluginInfo['def_desc'] ) ) ? '' : esc_textarea( $pluginInfo['def_desc'] ) ;
  $plugin_def_update_ready = ( empty($pluginInfo['def_update_ready'] ) ) ? 0 : 1 ;
  ?>
  <table id="plugin-info">
    <tr>
      <th>Plugin Name:</th>
      <td>
        <input type="text" name="plugin_def_name" id="plugin_def_name" class="regular-text" value="<?php echo $plugin_def_name; ?>"><br>
        Leave blank if the same as page/product name on top
      </td>
    </tr>
    <tr>
      <th>*Plugin Description:</th>
      <td>
        <textarea name="plugin_def_desc" id="plugin_def_desc" class="" cols="50" rows="3"><?php echo $plugin_def_desc; ?></textarea><br>
        Required
      </td>
    </tr>
    <tr>
      <th>Auto Update Ready</th>
      <td>
        <input type="checkbox" id="plugin_def_update_ready" name="plugin_def_update_ready" value="1" <?php checked( $plugin_def_update_ready, 1, true ); ?>> <label for="plugin_def_update_ready">Yes</label>
      </td>
    </tr>
  </table>
  <?php
}

function pm_pdlp_delivery_mbox( $post, $args ) {
  wp_nonce_field( plugin_basename( __FILE__ ), 'pm_pdlp_nonce' );
  $pm_pdlp_opts = $args['args'][0];
  $aws3 = new pmAwsTasks('pm_pdlp_options', $args['args'][1], $args['args'][2]);
  $pm_pdlp_pu_updater = get_post_meta( $post->ID, 'pm_pdlp_pu_updater', true );
  $pm_pdlp_pu_update_check_url = ( empty( $pm_pdlp_pu_updater['update_check_url'] ) ) ? '' : $pm_pdlp_pu_updater['update_check_url'] ;
  $pm_pdlp_pu_plugin_update_url = ( empty( $pm_pdlp_pu_updater['plugin_update_url'] ) ) ? '' : $pm_pdlp_pu_updater['plugin_update_url'] ;
  ?>
  <table id="fileselect">
    <tr>
      <th>Plugin To Deliver:</th>
      <td>
        <select name="pm_pdlp_pluginfilename">
          <option value="0">None</option>
        <?php
        if( !empty( $pm_pdlp_opts['defbucket'] ) ) {
          $aws3->pm_pdlp_getfiles( $pm_pdlp_opts['defbucket'], $pm_pdlp_opts['bucketfolder'], 'select', $post->ID, 'pm_pdlp_pluginfilename' );
        }
        ?>
        </select>
      </td>
    </tr>
    <tr class="filetableheading">
      <th colspan="2">Personal Use Files & Access</th>
    </tr>
    <tr>
      <th>User Guide To Deliver:</th>
      <td>      
        <select name="pm_pdlp_pu_guide_filename">
        <option value="0">None</option>
        <?php
        if( !empty( $pm_pdlp_opts['defbucket'] ) ) {
          $aws3->pm_pdlp_getfiles( $pm_pdlp_opts['defbucket'], $pm_pdlp_opts['pu_bucketfolder'], 'select', $post->ID, 'pm_pdlp_pu_guide_filename' );
        }
        ?>
        </select>
      </td>
    </tr>
    <tr>
      <th>Automatic Update Check URL</th>
      <td>
        <input type="text" name="pm_pdlp_pu_update_check_url" class="large-text" value="<?php echo $pm_pdlp_pu_update_check_url; ?>">
      </td>
    </tr>
    <tr>
      <th>Automatic Plugin Update URL</th>
      <td>
        <input type="text" name="pm_pdlp_pu_plugin_update_url" class="large-text" value="<?php echo $pm_pdlp_pu_plugin_update_url; ?>">
      </td>
    </tr>
    <tr>
      <th>Grant personal use access to buyers of:</th>
      <td>
        <?php
        // Get products from aMemberFuncs.php
        pm_pdlp_getAmbrProducts($post->ID, 'pm_pdlp_personaluse_pid');
        ?>
      </td>
    </tr>
    <tr class="filetableheading">
      <th colspan="2">Private Label Files & Access</th>
    </tr>
    <tr>
      <th>Selling Tools To Deliver:</th>
      <td>
        <select name="pm_pdlp_toolsfilename">
        <option value="0">None</option>
        <?php
        if( !empty( $pm_pdlp_opts['defbucket'] ) ) {
          $aws3->pm_pdlp_getfiles( $pm_pdlp_opts['defbucket'], $pm_pdlp_opts['bucketfolder'], 'select', $post->ID, 'pm_pdlp_toolsfilename' );
        }
        ?>
        </select>
      </td>
    </tr>
    <tr>
      <th>Grant access to buyers of:</th>
      <td>
        <?php
        // Get products from aMemberFuncs.php
        pm_pdlp_getAmbrProducts($post->ID, 'pm_pdlp_pid');
        ?>
      </td>
    </tr>
  </table>
  <div id="s3upload" style="display:none;">
     This is my hidden content! It will appear in ThickBox when the link is clicked.
</div>
  <?php
}
function pm_pdlp_salestatus_mbox( $post ) {
  wp_nonce_field( plugin_basename( __FILE__ ), 'pm_pdlp_nonce' );
  $pluginSaleStatus = get_post_meta( $post->ID, 'pm_pdlp_sales_status', true );
  $pluginInfo = get_post_meta( $post->ID, 'pm_pdlp_plugin_info', true );
  $pluginFile = get_post_meta( $post->ID, 'pm_pdlp_pluginfilename', true );
  $toolsFile = get_post_meta( $post->ID, 'pm_pdlp_toolsfilename', true );
  $pm_pdlp_pid = get_post_meta( $post->ID, 'pm_pdlp_pid', true );
  $crosssell = get_post_meta( $post->ID, 'crosssell', true );
  $salesStatus = get_post_meta( $post->ID, 'pm_pdlp_salestatus', true );
  ?>
  <div class="current-status">
    <?php if( $salesStatus == 0 ) { ?>
    <p id="stat-not-ready" class="stage-0"><i class="pdlp-thumbs-down"></i> Not Ready</p>
    <?php } else { ?>
    <p id="stat-ready" class="stage-0"><i class="pdlp-thumbs-up"></i> Ready</p>
    <?php } ?>
  </div>
  <p>Plugin name: <?php if( empty( $post->post_title ) ) { ?><i class="pdlp-cancel"></i><?php } else { ?><i class="pdlp-ok"></i><?php } ?><br>
  Description: <?php if( empty( $pluginInfo['def_desc'] ) ) { ?><i class="pdlp-cancel"></i><?php } else { ?><i class="pdlp-ok"></i><?php } ?><br>
  Plugin file: <?php if( empty( $pluginFile ) ) { ?><i class="pdlp-cancel"></i><?php } else { ?><i class="pdlp-ok"></i><?php } ?><br>
  Selling tools: <?php if( empty( $toolsFile ) ) { ?><i class="pdlp-cancel"></i><?php } else { ?><i class="pdlp-ok"></i><?php } ?><br>
  Access granted: <?php if( empty( $pm_pdlp_pid ) ) { ?><i class="pdlp-cancel"></i><?php } else { ?><i class="pdlp-ok"></i><?php } ?><br>
  Custom cross sell: <?php if( empty( $crosssell ) ) { ?><i class="pdlp-cancel stage-1"></i><?php } else { ?><i class="pdlp-ok"></i><?php } ?></p>
  <?php
}

// WP editor for custom promo
add_action( 'edit_form_advanced', 'pm_pdlp_custom_crosssell' );
function pm_pdlp_custom_crosssell( $post ) {
  wp_nonce_field( plugin_basename( __FILE__ ), 'pm_pdlp_nonce' );
  //$pm_pdlp_opts = get_option( 'pm_pdlp_options' );
  $metadata = get_post_meta( $post->ID, 'crosssell', true );
  $crosssell = ( empty( $metadata ) ) ? '' : stripslashes( $metadata );
  $settings = array(
    'textarea_rows' => 5
    );
  echo '<h2>Custom Cross Sell</h2>';
    wp_editor( $crosssell, 'crosssell', $settings );
}

// WP editor for changelog
add_action( 'edit_form_advanced', 'pm_pdlp_changelog' );
function pm_pdlp_changelog( $post ) {
  wp_nonce_field( plugin_basename( __FILE__ ), 'pm_pdlp_nonce' );
  $metadata = get_post_meta( $post->ID, 'changelog', true );
  $changelog = ( empty( $metadata ) ) ? '' : stripslashes( $metadata );
  $settings = array(
    'textarea_rows' => 5
    );
  echo '<h2>Change Log</h2>';
    wp_editor( $changelog, 'changelog', $settings );
}

// Save post & validate
add_action( 'save_post', 'pm_pdlp_save_postdata' );
function pm_pdlp_save_postdata( $post_id ) {
  if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) 
      return;
  if ( isset($_POST['pm_pdlp_nonce']) && !wp_verify_nonce( $_POST['pm_pdlp_nonce'], plugin_basename( __FILE__ ) ) )
      return;
  if ( isset($_POST['post_type']) && 'page' == $_POST['post_type'] ) 
  {
    if ( !current_user_can( 'edit_page', $post_id ) )
        return;
  }
  else
  {
    if ( !current_user_can( 'edit_post', $post_id ) )
        return;
  }

  if( get_post_type( $post_id ) == 'pm_pdlp'  ){
    $valid_plugin_def_name = ( empty( $_POST['plugin_def_name'] ) ) ? '' : sanitize_text_field( $_POST['plugin_def_name'] ) ;
    $valid_plugin_def_desc = ( empty( $_POST['plugin_def_desc'] ) ) ? '' : sanitize_text_field( $_POST['plugin_def_desc'] ) ;
    $valid_plugin_def_update_ready = ( empty( $_POST['plugin_def_update_ready'] ) ) ? '' : 1 ;
    //$valid_pm_pdlp_pid = ( is_numeric( $_POST['pm_pdlp_pid'] ) ) ? $_POST['pm_pdlp_pid'] : '0' ;
    $valid_pm_pdlp_pluginfilename = ( empty( $_POST['pm_pdlp_pluginfilename'] ) ) ? '' : sanitize_text_field( $_POST['pm_pdlp_pluginfilename'] ) ;
    $valid_pm_pdlp_pu_guide_filename = ( empty( $_POST['pm_pdlp_pu_guide_filename'] ) ) ? '' : sanitize_text_field( $_POST['pm_pdlp_pu_guide_filename'] ) ;
    $valid_pm_pdlp_pu_plugin_update_url = ( empty( $_POST['pm_pdlp_pu_plugin_update_url'] ) ) ? '' : esc_url_raw( $_POST['pm_pdlp_pu_plugin_update_url'] ) ;
    $valid_pm_pdlp_pu_update_check_url = ( empty( $_POST['pm_pdlp_pu_update_check_url'] ) ) ? '' : esc_url_raw( $_POST['pm_pdlp_pu_update_check_url'] ) ;

    $valid_pm_pdlp_toolsfilename = ( empty( $_POST['pm_pdlp_toolsfilename'] ) ) ? '' : sanitize_text_field( $_POST['pm_pdlp_toolsfilename'] ) ;
    $valid_pm_pdlp_crosssell = ( empty( $_POST['crosssell'] ) ) ? '' : wp_kses_post( $_POST['crosssell'] ) ;
    $valid_pm_pdlp_changelog = ( empty( $_POST['changelog'] ) ) ? '' : wp_kses_post( $_POST['changelog'] ) ;
    // Validate each personal use product id
    if( !empty($_POST['pm_pdlp_personaluse_pid'] ) ) {
      foreach ($_POST['pm_pdlp_personaluse_pid'] as $k => $v) {
        if( is_numeric( $v ) ) {
          $valid_pm_pdlp_personaluse_pid[] = $v;
        }
      }
    }
    // Validate each plr product id
    if( !empty($_POST['pm_pdlp_pid'] ) ) {
      foreach ($_POST['pm_pdlp_pid'] as $k => $v) {
        if( is_numeric( $v ) ) {
          $valid_pm_pdlp_pid[] = $v;
        }
      }
    }
    //Set readiness
    if( empty( $_POST['post_title'] ) || empty($valid_plugin_def_desc) || empty($valid_pm_pdlp_pluginfilename) || empty($valid_pm_pdlp_toolsfilename) || empty($_POST['pm_pdlp_pid']) ) {
      $valid_sales_status = '0';
    } else {
      $valid_sales_status = '1';
    }
    
    update_post_meta( $post_id, 'pm_pdlp_plugin_info', array( 'def_name' => $valid_plugin_def_name, 'def_desc' => $valid_plugin_def_desc, 'def_update_ready' => $valid_plugin_def_update_ready ) );
    update_post_meta( $post_id, 'pm_pdlp_personaluse_pid', $valid_pm_pdlp_personaluse_pid );
    update_post_meta( $post_id, 'pm_pdlp_pid', $valid_pm_pdlp_pid );
    update_post_meta( $post_id, 'pm_pdlp_pluginfilename', $valid_pm_pdlp_pluginfilename );
    update_post_meta( $post_id, 'pm_pdlp_pu_guide_filename', $valid_pm_pdlp_pu_guide_filename );
    update_post_meta( $post_id, 'pm_pdlp_pu_updater', array( 'update_check_url' => $valid_pm_pdlp_pu_update_check_url, 'plugin_update_url' => $valid_pm_pdlp_pu_plugin_update_url ) );
    update_post_meta( $post_id, 'pm_pdlp_toolsfilename', $valid_pm_pdlp_toolsfilename );
    update_post_meta( $post_id, 'crosssell', $valid_pm_pdlp_crosssell );
    update_post_meta( $post_id, 'changelog', $valid_pm_pdlp_changelog );
    update_post_meta( $post_id, 'pm_pdlp_salestatus', $valid_sales_status );
  }  
}