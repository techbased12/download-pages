<?php
/* References:
 * http://aws.amazon.com/code/1093
 * http://www.spacevatican.org/2013/7/7/direct-to-s3-browser-uploads/
 * http://birkoff.net/blog/post-files-directly-to-s3-with-php/
 * http://stackoverflow.com/questions/10774293/posting-form-data-to-amazon-s3-bucket
 * http://stackoverflow.com/a/14520473
 */
// this file contains the contents of the popup window
$wpdir = dirname(dirname(dirname(dirname(dirname(dirname(__FILE__))))));
include($wpdir.'/wp-load.php');

// Get default options
$pm_pdlp_opts = get_option( 'pm_pdlp_options' );
$now = strtotime(date("Y-m-d\TG:i:s")); 
$expiration = date('Y-m-d\TG:i:s\Z', strtotime('+ 1 hours', $now)); // credentials valid 1 hour from now

$form = array(
            'key'						=> $pm_pdlp_opts['bucketfolder'].'/text.txt',
            'AWSAccessKeyId'            => $pm_pdlp_opts['amzaccess'],
            'aws_secret_key'			=> $pm_pdlp_opts['amzsecret'],
            'acl'                       => 'private',
            'success_action_redirect'   => '',
            'bucket'					=> $pm_pdlp_opts['defbucket'],
            'bucketfolder'				=> $pm_pdlp_opts['bucketfolder']
        );
$key = $form['key'];

$form['policy'] = '{
    "expiration": "'.$expiration.'",
        "conditions": [
            {
                "acl": "'.$form['acl'].'"
            },
            {
                "bucket": "'.$form['bucket'].'"
            },
            {
            	"success_action_status": "201"
            },
            [
                "starts-with",
                "$key",
                "'.$form['bucketfolder'].'/"
	        ]
        ]
    }';

    $form['policy_encoded'] = base64_encode($form['policy']);
    //$form['signature'] = base64_encode(hash_hmac( 'sha1', base64_encode(utf8_encode($form['policy'])), 'tRiqKfLlzP9kQSrA+Ym7H4zr1yHSHNrhFNyOJwxk', true));
    $form['signature'] = base64_encode(hash_hmac('sha1', $form['policy_encoded'], $form['aws_secret_key'], true));
?>
<html>
<head>
	<title>Upload Media</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link type="text/css" rel="stylesheet" media="all" href="<?php echo PM_PDLP_DIR; ?>modules/css/bootstrap.min.css">
	<link type="text/css" rel="stylesheet" media="all" href="<?php echo PM_PDLP_DIR; ?>modules/css/bootstrap-theme.min.css">
</head>
<body id="pdlp-upload-dialog">
<div class="container">
	<p class="lead">Click Browse... to select a file to upload.</p>
	<small>Note: For multiple or very large files, we highly recommend using a desktop upload tool or logging into your Amazon S3 account.</small>
	<form id="upload" action="http://<?php echo $form['bucket']; ?>.s3.amazonaws.com" method="post" enctype="multipart/form-data">
		<input type="hidden" name="key" value="<?php echo $form['bucketfolder'] ?>/${filename}">
		<input type="hidden" name="acl" value="<?php echo $form['acl']; ?>">
		<input type="hidden" name="AWSAccessKeyId" value="<?php echo $form['AWSAccessKeyId']; ?>">
		<input type="hidden" name="policy" value="<?php echo $form['policy_encoded']; ?>">
		<input type="hidden" name="signature" value="<?php echo $form['signature']; ?>">
		<input type="hidden" name="success_action_status" value="201">
		<input type="file" name="file">
		<input type="submit" name="submit" value="Upload to Amazon S3" class="btn btn-primary">
	</form>
	<div id="uploadwait"></div>
	<div id="uploadresponse" style="display: none;">
		<p id="successresponse" class="alert alert-success"></p>
		<p style="text-align: center;" class="btn btn-primary" id="closerefresh">Close and refresh page</p>
	</div>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script>
	jQuery(document).ready( function($){
	$(document).on('submit', 'form#upload', function(event) {
		event.preventDefault();
		
		var $form = $(this).closest('form');
		//grab all form data  
  		var formData = new FormData($(this)[0]);

  		$('#uploadwait').html('<img src="../images/stat.gif">');

		$.ajax({
	        type: $form.attr('method'),
	        //dataType: 'html',
	        url: $form.attr('action'),
	        data: formData,
	        processData: false,
	        contentType: false,
	        success: function(xml) {
	        	var location = $(xml).find('Location').first().text();
	        	var key = $(xml).find('Key').first().text();
	        	if( location ) {
	        		$('form#upload').hide();
	        		$('#uploadwait').hide();
	        		$('#uploadresponse').show();
		            $('#successresponse').append('The file '+ key +' was uploaded successfully.');
		            self.parent.location.reload();
		        }
	        }

	    });
	});
	$('#closerefresh').click(function(){
		self.parent.location.reload();
		self.parent.tb_remove();
	});
	}); // global end
</script>
</div>
</body>
</html>