jQuery(document).ready( function($){
	// Popup if user has no access
	$('a.noaccess').magnificPopup({
		type: 'inline',
		midclick: true
	});

	$('#add-updater').on('click', function(e) {
		if( $('#add-updater').prop('checked') ) {
			$('label[for="update-check-url"]').show();
			$('#update-check-url').show();
			$('#update-check-url').addClass('update-check-url-highlight');
			$('#warning-update-check-url').show();
			$('label[for="updated-plugin-url"]').show();
			$('#updated-plugin-url').show();
		} else {
			$('label[for="update-check-url"]').hide();
			$('#update-check-url').hide();
			$('#update-check-url').removeClass('update-check-url-highlight');
			$('#warning-update-check-url').hide();
			$('label[for="updated-plugin-url"]').hide();
			$('#updated-plugin-url').hide();
		}
	});

	$('#rebranderSubmit ').on('click', function(e){
		e.preventDefault();

		// Set plugin name value
		if( $('#plugin-name').val() == '' ) {
			var pluginName = '';
		} else {
			var pluginName = $('input[name=pluginName]').val();
		}
		
		// Set plugin URI
		if( $('#plugin-uri').val() == '' ) {
			var pluginUri = '';
		} else {
			var pluginUri = $('input[name=pluginUri]').val();
		}
		
		// Set plugin description
		if( $('#plugin-desc').val() == '' ) {
			var pluginDesc = $('textarea[name=pluginDesc]').attr('placeholder');
		} else {
			var pluginDesc = $('textarea[name=pluginDesc]').val();
		}
		
		// Set plugin author name
		if( $('#author-name').val() == '' ) {
			var authorName = '';
		} else {
			var authorName = $('input[name=authorName]').val();
		}
		
		// Set author url
		if( $('#author-uri').val() == '' ) {
			var authorUri = '';
		} else {
			var authorUri = $('input[name=authorUri]').val();
		}

		// Check for update checker inclusion
		if( $('#add-updater').prop('checked') || $('#add-updater').val() == '1' ) {
				var addUpdater = 1;
		} else {
			var addUpdater = 0;
		}

		// Check for update checker URL
		if( $('#update-check-url').val() == '' ) {
			var updateCheckUrl = '';
		} else {
			var updateCheckUrl = $('input[name=updateCheckUrl]').val();
		}

		// Check for updated plugin URL
		if( $('#updated-plugin-url').val() == '' ) {
			var updatedPluginUrl = '';
		} else {
			var updatedPluginUrl = $('input[name=updatedPluginUrl]').val();
		}

		// Check for personal use override
		if( $('input[name=pu_override]').val() == '1' ) {
			var pu_override = 1;
		} else {
			var pu_override = 0;
		}

		// Response
		$('form#pmillrebrander').hide();
		$('#rebrandProcess').show();
		//console.log($('#plugin-desc').val());
		//console.log('name: '+pluginName+' URI: '+pluginUri+' Description: '+pluginDesc+' Author: '+authorName+' Author URI: '+authorUri);

		$.post(
			pdlp.ajaxurl,
			{
				action: 'pmillrebrander',
				pdlpNonce: pdlp.pdlpNonce,
				pluginName: pluginName,
				pluginUri: pluginUri,
				pluginDesc: pluginDesc,
				authorName: authorName,
				authorUri: authorUri,
				placeholderPluginName: $('input[name=pluginName]').attr('placeholder'),
				addUpdater: addUpdater,
				updateCheckUrl: updateCheckUrl,
				updatedPluginUrl: updatedPluginUrl,
				pu_override: pu_override,
				post_id: $('input[name=post_id]').val()
			},
			function ( response ) {
				$('#rebrandResponse').html( response );				
				$('#rebrandProcess').hide();
				//console.log( response );
			}
			);
		return false;
	});
}); // global end