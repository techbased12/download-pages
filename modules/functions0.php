<?php
// Display custom template for download pages
add_filter( 'template_include', 'pm_pdlp_customTemplate' );
function pm_pdlp_customTemplate( $template ) {
	global $wp_query;
	
	// Example for custom post type
	if( ( get_query_var('post_type') == 'pm_pdlp' ) || ( is_front_page() && is_singular('pm_pdlp') ) ) {
		$template = PM_PDLP_PATH .'/modules/templates/default.php';

	}

	return $template;
}

// Ajax actions
add_action( 'wp_ajax_pmillrebrander', 'pm_pdlp_action_function' );
add_action( 'wp_ajax_nopriv_pmillrebrander', 'pm_pdlp_action_function');
function pm_pdlp_action_function() {
	$pm_pdlp_opts = get_option( 'pm_pdlp_options' );
	$post_id = ( is_numeric( $_POST['post_id'] ) ) ? sanitize_text_field( $_POST['post_id'] ) : '' ;
	$pluginFileName =  get_post_meta( $post_id, 'pm_pdlp_pluginfilename', true );
	$toolsFileName =  get_post_meta( $post_id, 'pm_pdlp_toolsfilename', true );
	// Check nonce
	$nonce = $_POST['pdlpNonce'];
	if( !wp_verify_nonce( $nonce, 'pmillrebrander' ) )
		die('Busted!');

	// Check if fields are empty
	if( empty( $_POST['pluginName'] ) || empty( $_POST['pluginUri'] ) || empty( $_POST['pluginDesc'] ) || empty( $_POST['authorName'] ) || empty( $_POST['authorUri'] ) ){
			echo '<span class="error">All fields are required. Please check your submission again.</span>';
	} else {
	// Validate/Sanitize Data
	$new_pluginName = sanitize_text_field( $_POST['pluginName'] );
	$new_pluginUri = sanitize_text_field( $_POST['pluginUri'] );
	$new_pluginDesc = wp_kses_post( $_POST['pluginDesc'] );
	$new_authorName = sanitize_text_field( $_POST['authorName'] );
	$new_authorUri = sanitize_text_field( $_POST['authorUri'] );

	// Build Amazon signed URL
	$signedURL = new pmill_getAzSignedUrl($pm_pdlp_opts['defbucket'], $pm_pdlp_opts['bucketfolder'], $pluginFileName.'.zip', $pm_pdlp_opts['defexpiry'], $pm_pdlp_opts['amzaccess'], $pm_pdlp_opts['amzsecret']);

	// Perform rebranding
	pm_pdlp_brandit( $signedURL->signedUrl(), $pluginFileName, $new_pluginName, $new_pluginUri, $new_pluginDesc, $new_authorName, $new_authorUri );

	// Generate response
	/*
	$r = array (
		'post_id' => $post_id,
		'signedURL' => $signedURL->signedUrl(),
		'pluginFileName' => $pluginFileName,
		'pluginName' => $new_pluginName
	);
	$response = json_encode( $r );

	// response output
	echo $response;
	*/
	}
	exit();
}

/* * * * * * * * * * * * *
 * Copy and brand plugin
 * * * * * * * * * * * * */

// Class to get Amazon Signed URL
class pmill_getAzSignedUrl {
	// Properties
	private $bucket;
	private $folder;
	private $file;
	private $expiry;
	private $access_key;
	private $secret;

	// Assigning values
	public function __construct( $bucket, $folder, $file, $expiry, $access_key, $secret ) {
		$this->bucket = stripslashes( $bucket );
		$this->file = stripslashes( $file );
		$this->folder = stripslashes( $folder );
		$this->expiry = (is_numeric( $expiry ) ) ? $expiry : '0' ;
		$this->access_key = stripslashes( $access_key );
		$this->secret = stripslashes( $secret );
	}

	// Create method (function)
	public function signedUrl() {
		// Expiry time
		$expiry = time()+$this->expiry*60;
		// Sign the URL string
		$string_to_sign = "GET\n\n\n$expiry\n/".str_replace(".s3.amazonaws.com","",$this->bucket)."/$this->folder/$this->file";
	    $signature = urlencode(base64_encode((hash_hmac("sha1", utf8_encode($string_to_sign), $this->secret, TRUE))));

		return 'http://'.$this->bucket.'.s3.amazonaws.com/'.$this->folder.'/'.$this->file.'?AWSAccessKeyId='.$this->access_key.'&Expires='.$expiry.'&Signature='.$signature;
	}
}

// Function to do actual branding
function pm_pdlp_brandit( $sourcePlugin, $pluginFileName, $PluginName, $PluginUri, $PluginDesc, $PluginAuthor, $AuthorUri ) {
	$tmpDirName = uniqid();
	$fileName = preg_replace('/-\d\.\d{1,2}\.\d{1,2}/', '', $pluginFileName);
	$versionNumber = substr($pluginFileName, -5);
	$newPluginFileName = preg_replace('/\s+/', '_', $PluginName);

	$copiedPlugin = PM_PDLP_PATH.'/modules/deliver/'.$tmpDirName.'-'.$newPluginFileName.'-'.$versionNumber.'.zip';

	if( copy($sourcePlugin, $copiedPlugin) ) {
		//echo '<p>Plugin Copied</p>';
	} else {
		echo '<p>Could not copy plugin</p>';
		//echo '<p>Source: '.$sourcePlugin.'</p>';
	}

	// Create object
	$zip = new ZipArchive();
	$pluginIndex = $fileName.'/'.$fileName.'.php';

	//Open archive
	if ( $zip->open($copiedPlugin) === TRUE ) {
		//echo '<p>Getting ready to rebrand</p>';
		// Get file from zip
		$findIndex = $zip->getFromName( $pluginIndex );
		// Modify file
		$mergecodes = array(
			'__PluginName__',
			'__PluginUri__',
			'__PluginDesc__',
			'__PluginAuthor__',
			'__AuthorUri__'
			);
		$brandinfo = array(
			$PluginName,
			$PluginUri,
			$PluginDesc,
			$PluginAuthor,
			$AuthorUri
			);
		$newFile = str_replace( $mergecodes, $brandinfo, $findIndex );
		// Delete original file
		$zip->deleteName( $pluginIndex );
		// Replace with new file created
		$zip->addFromString( $pluginIndex, $newFile );
		// Close
		$zip->close();
		echo '<p>All done! Click the button below to begin downloading.</p><p class="center"><a href="'.PM_PDLP_DIR.'modules/deliver/'.$tmpDirName.'-'.$newPluginFileName.'-'.$versionNumber.'.zip" class="pmill-btn download">Download Plugin</a>.</p>';
	} else {
		die( 'Could not open plugin archive.' );
	}
}

// Add upload button
add_action( 'media_buttons','pm_pdlp_s3upload',100);
function pm_pdlp_s3upload(){
    global $pagenow,$typenow;

    if (!in_array( $pagenow, array( 'post.php', 'post-new.php' ) ))
        return;
    if( $typenow != 'pm_pdlp' )
    	return;

    echo '<a href="'.PM_PDLP_DIR.'modules/libs/s3-upload-dialog.php?TB_iframe=true&height=155&width=300&inlineId=s3upload" title="Upload to S3" class="thickbox button"><span class="dashicons dashicons-upload"></span>Upload To S3</a>';
}

// Flatten array
function pm_pdlp_flatten($array) {
	$result = '';
    foreach ($array as $key => $value) {
    	$result = $value.",";
    }
    return $result;
}
?>