<?php 
add_action( 'add_meta_boxes', 'pm_pdlp_add_custom_box' ); // Called in register_post_type
function pm_pdlp_add_custom_box() {
  if( class_exists('Symfony\Component\ClassLoader\UniversalClassLoader') ) {
    // Get default options
    $pm_pdlp_opts = get_option('pm_pdlp_options');

    // Filter and sort out options
    $access_key = sanitize_text_field( $pm_pdlp_opts['amzaccess'] );
    $secret = sanitize_text_field( $pm_pdlp_opts['amzsecret'] );

    if( !empty($access_key) || !empty( $secret ) ) {
      $s3Client = Aws\Common\Aws::factory(array(
        'key' => $access_key,
        'secret' => $secret,
        ));
      $s3 = $s3Client->get('s3');
      // Get buckets & files
      $allbuckets = $s3->listBuckets()->toArray();
      $buckets = $allbuckets['Buckets'];
    }
  
    $args = array( $pm_pdlp_opts, $buckets, $s3 );
  }

	// Load metabox for specified post type only
    add_meta_box(
      'pm_pdlp_info',
      __( 'Plugin Information', 'pm-download-pages' ),
      'pm_pdlp_info_mbox',
      'pm_pdlp', 'normal', 'default',
      $post
      );
    add_meta_box(
      'pm_pdlp_delivery',
      __( 'Product Files & Access', 'pm-download-pages' ),
      'pm_pdlp_delivery_mbox',
      'pm_pdlp', 'normal', 'default',
      $args
      );
}

// Metabox callback
function pm_pdlp_info_mbox( $post ) {
  wp_nonce_field( plugin_basename( __FILE__ ), 'pm_pdlp_nonce' );
  $pluginInfo = get_post_meta( $post->ID, 'pm_pdlp_plugin_info', true );
  $plugin_def_name = ( empty($pluginInfo['def_name'] ) ) ? '' : esc_attr( $pluginInfo['def_name'] ) ;
  $plugin_def_desc = ( empty($pluginInfo['def_desc'] ) ) ? '' : esc_textarea( $pluginInfo['def_desc'] ) ;
  ?>
  <table id="plugin-info">
    <tr>
      <th>Plugin Name:</th>
      <td>
        <input type="text" name="plugin_def_name" id="plugin_def_name" class="regular-text" value="<?php echo $plugin_def_name; ?>"><br>
        Leave blank if the same as page/product name on top
      </td>
    </tr>
    <tr>
      <th>*Plugin Description:</th>
      <td>
        <textarea name="plugin_def_desc" id="plugin_def_desc" class="" cols="50" rows="3"><?php echo $plugin_def_desc; ?></textarea><br>
        Required
      </td>
    </tr>
  </table>
  <?php
}

function pm_pdlp_delivery_mbox( $post, $args ) {
  wp_nonce_field( plugin_basename( __FILE__ ), 'pm_pdlp_nonce' );
  $pm_pdlp_opts = $args['args'][0];
  $aws3 = new pmAwsTasks('pm_pdlp_options', $args['args'][1], $args['args'][2]);
  ?>
  <table id="fileselect">
    <tr>
      <th>Plugin To Deliver:</th>
      <td>
        <select name="pm_pdlp_pluginfilename">
          <option value="0">None</option>
        <?php
        if( !empty( $pm_pdlp_opts['defbucket'] ) ) {
          $aws3->pm_pdlp_getfiles( $pm_pdlp_opts['defbucket'], 'select', $post->ID, 'pm_pdlp_pluginfilename' );
        }
        ?>
        </select>
      </td>
    </tr>
    <tr>
      <th>Selling Tools To Deliver:</th>
      <td>
        <select name="pm_pdlp_toolsfilename">
        <option value="0">None</option>
        <?php
        if( !empty( $pm_pdlp_opts['defbucket'] ) ) {
          $aws3->pm_pdlp_getfiles( $pm_pdlp_opts['defbucket'], 'select', $post->ID, 'pm_pdlp_toolsfilename' );
        }
        ?>
        </select>
      </td>
    </tr>
    <tr>
      <th>Grant access to buyers of:</th>
      <td>
        <?php
        // Get products from aMemberFuncs.php
        pm_pdlp_getAmbrProducts($post->ID, 'pm_pdlp_pid');
        ?>
      </td>
    </tr>
  </table>
  <div id="s3upload" style="display:none;">
     This is my hidden content! It will appear in ThickBox when the link is clicked.
</div>
  <?php
}
// WP editor for custom promo
add_action( 'edit_form_advanced', 'pm_pdlp_custom_crosssell' );
function pm_pdlp_custom_crosssell( $post ) {
  wp_nonce_field( plugin_basename( __FILE__ ), 'pm_pdlp_nonce' );
  //$pm_pdlp_opts = get_option( 'pm_pdlp_options' );
  $metadata = get_post_meta( $post->ID, 'crosssell', true );
  $crosssell = ( empty( $metadata ) ) ? '' : stripslashes( $metadata );
  $settings = array(
    'textarea_rows' => 5
    );
  echo '<h2>Custom Cross Sell</h2>';
    wp_editor( $crosssell, 'crosssell', $settings );
}

// Save post & validate
add_action( 'save_post', 'pm_pdlp_save_postdata' );
function pm_pdlp_save_postdata( $post_id ) {
  if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) 
      return;
  if ( isset($_POST['pm_pdlp_nonce']) && !wp_verify_nonce( $_POST['pm_pdlp_nonce'], plugin_basename( __FILE__ ) ) )
      return;
  if ( isset($_POST['post_type']) && 'page' == $_POST['post_type'] ) 
  {
    if ( !current_user_can( 'edit_page', $post_id ) )
        return;
  }
  else
  {
    if ( !current_user_can( 'edit_post', $post_id ) )
        return;
  }

  if( get_post_type( $post_id ) == 'pm_pdlp'  ){
    $valid_plugin_def_name = ( empty( $_POST['plugin_def_name'] ) ) ? '' : sanitize_text_field( $_POST['plugin_def_name'] ) ;
    $valid_plugin_def_desc = ( empty( $_POST['plugin_def_desc'] ) ) ? '' : sanitize_text_field( $_POST['plugin_def_desc'] ) ;
    //$valid_pm_pdlp_pid = ( is_numeric( $_POST['pm_pdlp_pid'] ) ) ? $_POST['pm_pdlp_pid'] : '0' ;
    $valid_pm_pdlp_pluginfilename = ( empty( $_POST['pm_pdlp_pluginfilename'] ) ) ? '' : sanitize_text_field( $_POST['pm_pdlp_pluginfilename'] ) ;
    $valid_pm_pdlp_toolsfilename = ( empty( $_POST['pm_pdlp_toolsfilename'] ) ) ? '' : sanitize_text_field( $_POST['pm_pdlp_toolsfilename'] ) ;
    $valid_pm_pdlp_crosssell = ( empty( $_POST['crosssell'] ) ) ? '' : wp_kses_post( $_POST['crosssell'] ) ;
    // Validate each product id
    if( !empty($_POST['pm_pdlp_pid'] ) ) {
      foreach ($_POST['pm_pdlp_pid'] as $k => $v) {
        if( is_numeric( $v ) ) {
          $valid_pm_pdlp_pid[] = $v;
        }
      }
    }

    update_post_meta( $post_id, 'pm_pdlp_plugin_info', array( 'def_name' => $valid_plugin_def_name, 'def_desc' => $valid_plugin_def_desc ) );
    update_post_meta( $post_id, 'pm_pdlp_pid', $valid_pm_pdlp_pid );
    update_post_meta( $post_id, 'pm_pdlp_pluginfilename', $valid_pm_pdlp_pluginfilename );
    update_post_meta( $post_id, 'pm_pdlp_toolsfilename', $valid_pm_pdlp_toolsfilename );
    update_post_meta( $post_id, 'crosssell', $valid_pm_pdlp_crosssell );
  }  
}
?>