<?php
/* References
 * https://github.com/chipbennett/oenology/blob/master/functions/options.php#L84
 * http://www.chipbennett.net/2011/02/17/incorporating-the-settings-api-in-wordpress-themes/7/
 */
/* Add admin menu */
add_action( 'admin_menu', 'pm_pdlp_admin_menu' );
function pm_pdlp_admin_menu() {
	global $pm_pdlp_opts;
	// Create options page
	$settings = add_submenu_page('edit.php?post_type=pm_pdlp', PM_PDLP_PLUGIN_NAME.' Settings', 'Settings', 'manage_options', 'pm_pdlp_settings', 'pm_pdlp_options_page');
  	
  	// Load admin scripts when above page is called
  	//add_action( 'load-'.$settings, 'pm_pdlp_adminscripts' );
}

// Enqueue admin scripts
function pm_pdlp_adminscripts() {
	//wp_enqueue_script( 'pm-snappro-admin', '//ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js', '', PM_PDLP_VERSION, false );
	//wp_enqueue_style( 'pm-snappro-admin', PM_PDLP_DIR.'modules/css/admin.css', '', PM_PDLP_VERSION, 'all' );
}

// Output the settings page
function pm_pdlp_options_page() {
	$tab = ( isset( $_GET['tab'] )  ? $_GET['tab'] : 'general' );
	$settings_section = 'pm_pdlp_settings_'.$tab.'_tab';
	?>
	<div class="wrap">
		<h2><?php echo PM_PDLP_PLUGIN_NAME; ?> Settings</h2>
		<?php settings_errors(  ); ?>
		<?php pm_pdlp_settings_page_tabs(); ?>

		<form action='options.php' method='post'>
			<?php 
			// Call functions to build form
			settings_fields( 'pm_pdlp_options' );
			//do_settings_sections( 'pm_pdlp_settings' );
			do_settings_sections( $settings_section );

			// Update settings based on tab			
			?>
			<p class="submit">
			<?php
			submit_button( 'Save Settings', 'primary', 'pm_pdlp_options[submit-'.$tab.']', false, array( 'id' => 'submit-'.$tab ) );
			submit_button( 'Reset', 'secondary', 'pm_pdlp_options[reset-'.$tab.']', false, array( 'id' => 'reset-'.$tab ) );
			?></p>
		</form>
	</div>
	<?php
}

// Create tabs
function pm_pdlp_get_settings_page_tabs() {
	$tabs = array(
		'general' => 'General',
		'amember' => 'Amember',
		'messages' => 'Messages'
		);
	return $tabs;
}

// HTML Markup for tabs
function pm_pdlp_settings_page_tabs( $current = 'general' ) {
	// Determind which tab to display
	if( isset( $_GET['tab'] ) ) :
		$current = $_GET['tab'];
	else:
		$current = 'general';
	endif;

	// Get list of tabs
	$tabs = pm_pdlp_get_settings_page_tabs();
	$links = array();

	// Create links for each tab
	foreach ($tabs as $tab => $name) :
		if( $tab == $current ) :
			$links[] = '<a class="nav-tab nav-tab-active" href="?post_type=pm_pdlp&page=pm_pdlp_settings&tab='.$tab.'">'.$name.'</a>';
		else :
			$links[] = '<a class="nav-tab" href="?post_type=pm_pdlp&page=pm_pdlp_settings&tab='.$tab.'">'.$name.'</a>';
		endif;
	endforeach;

	echo '<h2 class="nav-tab-wrapper">';
	foreach ( $links as $link ) {
		echo $link;
	}
	echo '</h2>';
}

// Register settings
add_action( 'admin_init', 'pm_pdlp_initialize_settings' );
function pm_pdlp_initialize_settings() {
	// Register fields
	register_setting( 
		'pm_pdlp_options',
		'pm_pdlp_options', 
		'pm_pdlp_validate_options' );

	// Add settings sections
	pm_pdlp_add_settings_sections();
	//add_settings_section( 'permanent', 'One Section', 'pm_pdlp_one_section_callback', 'pm_pdlp_settings' );

	// Add settings fields
	pm_pdlp_add_settings_fields();
	//add_settings_field( 'textfield', 'Text Field', 'pm_pdlp_textfield_field_callback', 'pm_pdlp_settings', 'permanent' );
}

/*---------------
 Sections
 ---------------*/
// Register multiple settings (called in admin_init)
function pm_pdlp_add_settings_sections() {
	// Array with settings identifiers
	$sects = array(
		'permanent' => array(
			'id' => 'permanent',
			'title' => 'Permanent Settings',
			'page' => 'pm_pdlp_settings_general_tab'
			),
		'ambrsect' => array(
			'id' => 'ambrsect',
			'title' => 'aMember Settings',
			'page' => 'pm_pdlp_settings_amember_tab'
			),
		'messages' => array(
			'id' => 'messages',
			'title' => 'Download Page Messages',
			'page' => 'pm_pdlp_settings_messages_tab'
			)
		);

	// Loop through each
	foreach ($sects as $s) {
		add_settings_section( 
			$s['id'].'_section',
			$s['title'].' Options',
			'pm_pdlp_'.$s['id'].'_section_callback',
			$s['page']
			);
	}
}

/*----------------
Sections callback
-----------------*/
// Section callback
function pm_pdlp_permanent_section_callback() {
	
}
function pm_pdlp_ambrsect_section_callback() {
	echo '<p>These only apply if you are using aMember for your shopping cart.</p>';
}

/*------------------
Fields
------------------*/
// Register multiple fields (called in admin_init)
function pm_pdlp_add_settings_fields () {
	if( class_exists('Symfony\Component\ClassLoader\UniversalClassLoader') ) {
		// Get default options
		$pm_pdlp_opts = get_option('pm_pdlp_options');

		// Filter and sort out options
		$access_key = sanitize_text_field( $pm_pdlp_opts['amzaccess'] );
		$secret = sanitize_text_field( $pm_pdlp_opts['amzsecret'] );

		if( !empty($access_key) || !empty( $secret ) ) {
			$s3Client = Aws\Common\Aws::factory(array(
				'key' => $access_key,
				'secret' => $secret,
				));
			$s3 = $s3Client->get('s3');
			// Get buckets & files
			$allbuckets = $s3->listBuckets()->toArray();
			$buckets = $allbuckets['Buckets'];
		}
	}
	
	// Array with field info
	$fields = array(
		'productslug' => array(
			'id' => 'productslug',
			'title' => 'Page Slug',
			'page' => 'pm_pdlp_settings_general_tab',
			'section' => 'permanent_section',
			'args' => array($pm_pdlp_opts)
			),
		'amzaccess' => array(
			'id' => 'amzaccess',
			'title' => 'Amazon Access Key ID',
			'page' => 'pm_pdlp_settings_general_tab',
			'section' => 'permanent_section',
			'args' => array($pm_pdlp_opts)
			),
		'amzsecret' => array(
			'id' => 'amzsecret',
			'title' => 'Amazon Secret Access Key',
			'page' => 'pm_pdlp_settings_general_tab',
			'section' => 'permanent_section',
			'args' => array($pm_pdlp_opts)
			),
		'defbucket' => array(
			'id' => 'defbucket',
			'title' => 'Default Bucket',
			'page' => 'pm_pdlp_settings_general_tab',
			'section' => 'permanent_section',
			'args' => array($pm_pdlp_opts,$buckets)
			),
		'bucketfolder' => array(
			'id' => 'bucketfolder',
			'title' => 'Private Label Folder',
			'page' => 'pm_pdlp_settings_general_tab',
			'section' => 'permanent_section',
			'args' => array($pm_pdlp_opts, $buckets, $s3)
			),
		'pu_bucketfolder' => array(
			'id' => 'pu_bucketfolder',
			'title' => 'Personal Use Folder',
			'page' => 'pm_pdlp_settings_general_tab',
			'section' => 'permanent_section',
			'args' => array($pm_pdlp_opts, $buckets, $s3)
			),
		'defexpiry' => array(
			'id' => 'defexpiry',
			'title' => 'Default Expiry',
			'page' => 'pm_pdlp_settings_general_tab',
			'section' => 'permanent_section',
			'args' => array($pm_pdlp_opts)
			),
		'del_opt_settings' => array(
			'id' => 'del_opt_settings',
			'title' => 'Delete Options Settings',
			'page' => 'pm_pdlp_settings_general_tab',
			'section' => 'permanent_section',
			'args' => array($pm_pdlp_opts)
			),
		'ambrdomain' => array(
			'id' => 'ambrdomain',
			'title' => 'aMember Domain',
			'page' => 'pm_pdlp_settings_amember_tab',
			'section' => 'ambrsect_section',
			'args' => array($pm_pdlp_opts)
			),
		'ambrfolder' => array(
			'id' => 'ambrfolder',
			'title' => 'aMember Folder',
			'page' => 'pm_pdlp_settings_amember_tab',
			'section' => 'ambrsect_section',
			'args' => array($pm_pdlp_opts)
			),
		'thanks' => array(
			'id' => 'thanks',
			'title' => 'Thanks Message',
			'page' => 'pm_pdlp_settings_messages_tab',
			'section' => 'messages_section',
			'args' => array($pm_pdlp_opts)
			),
		'crosssell' => array(
			'id' => 'crosssell',
			'title' => 'Cross & Upsells',
			'page' => 'pm_pdlp_settings_messages_tab',
			'section' => 'messages_section',
			'args' => array($pm_pdlp_opts)
			)
		);

	// Loop through each in array
	foreach ($fields as $f) {
		add_settings_field( 
			$f['id'],
			$f['title'],
			'pm_pdlp_'.$f['id'].'_field_callback',
			$f['page'],
			$f['section'],
			$f['args']
			);
	}
}

/*--------------------
Fields callback
--------------------*/
function pm_pdlp_productslug_field_callback($args) {
	// Be sure the field names are formatted for array!
	// Also be sure name does not have ''
	$pm_pdlp_opts = $args[0];
	$productslug = ( empty( $pm_pdlp_opts['productslug'] ) ) ? '' : $pm_pdlp_opts['productslug'];
	?>
	<input type="text" name="pm_pdlp_options[productslug]" value="<?php echo $productslug; ?>" id="productslug" class="regular-text" <?php if( $productslug != 'pm_pdlp' ) { ?>disabled<?php } ?>><br>
	<small>Choose carefully. This cannot and should not be changed later or it will break all your product pages and lose them in the admin.</small>
	<?php
}
function pm_pdlp_amzaccess_field_callback($args) {
	$pm_pdlp_opts = $args[0];
	$amzaccess = ( empty( $pm_pdlp_opts['amzaccess'] ) ) ? '' : stripslashes( $pm_pdlp_opts['amzaccess'] );
	?>
	<input type="password" name="pm_pdlp_options[amzaccess]" value="<?php echo $amzaccess; ?>" id="amzaccess" class="regular-text"><br>
	<small>First 4 characters of key is <?php echo substr( $amzaccess, 0, 4 ); ?></small>
	<?php
}
function pm_pdlp_amzsecret_field_callback($args) {
	$pm_pdlp_opts = $args[0];
	$amzsecret = ( empty( $pm_pdlp_opts['amzsecret'] ) ) ? '' : stripslashes( $pm_pdlp_opts['amzsecret'] );
	?>
	<input type="password" name="pm_pdlp_options[amzsecret]" value="<?php echo $amzsecret; ?>" id="amzsecret" class="regular-text"><br>
	<small>First 4 digits of key is <?php echo substr( $amzsecret, 0, 4 ); ?></small>
	<?php
}
function pm_pdlp_defbucket_field_callback($args) {
	if( class_exists( 'pmAwsTasks' ) ) {
		$pmAwsTasks = new pmAwsTasks('pm_pdlp_options', $args[1], '');
		$pmAwsTasks->pm_pdlp_listbuckets( 'pm_pdlp_options[defbucket]');
	} else {
		echo 'There\'s a problem accessing your buckets';
	}
	?><br>
	<small>Enter Amazon Access and Secret keys, and save once to obtain list of buckets.</small>
	<?php
}
function pm_pdlp_bucketfolder_field_callback($args) {
	$pm_pdlp_opts = $args[0];
	$defbucket = ( empty( $pm_pdlp_opts['defbucket'] ) ) ? '' : stripslashes( $pm_pdlp_opts['defbucket'] );
	if( class_exists( 'pmAwsTasks' ) ) {
		$pmAwsTasks = new pmAwsTasks('pm_pdlp_options', $args[1], $args[2]);
	}
	?>
	<select name="pm_pdlp_options[bucketfolder]">
		<option value="0">None</option>
	<?php
		$pmAwsTasks->pm_pdlp_getfolders( $defbucket, 'select', 'pm_pdlp_options[bucketfolder]');
	?>
	</select><br>
	<small>Enter Amazon Access and Secret keys, and save once to obtain list of folders.</small>
	<?php
}
function pm_pdlp_pu_bucketfolder_field_callback($args) {
	$pm_pdlp_opts = $args[0];
	$defbucket = ( empty( $pm_pdlp_opts['defbucket'] ) ) ? '' : stripslashes( $pm_pdlp_opts['defbucket'] );
	if( class_exists( 'pmAwsTasks' ) ) {
		$pmAwsTasks = new pmAwsTasks('pm_pdlp_options', $args[1], $args[2]);
	}
	?>
	<select name="pm_pdlp_options[pu_bucketfolder]">
		<option value="0">None</option>
	<?php
		$pmAwsTasks->pm_pdlp_getfolders( $defbucket, 'select', 'pm_pdlp_options[pu_bucketfolder]');
	?>
	</select><br>
	<small>Enter Amazon Access and Secret keys, and save once to obtain list of folders.</small>
	<?php
}
function pm_pdlp_defexpiry_field_callback($args) {
	$pm_pdlp_opts = $args[0];
	$defexpiry = ( empty( $pm_pdlp_opts['defexpiry'] ) ) ? '' : stripslashes( $pm_pdlp_opts['defexpiry'] );
	?>
	<input type="text" name="pm_pdlp_options[defexpiry]" value="<?php echo $defexpiry; ?>" id="defexpiry" class="small-text"> mins<br>
	<small>Enter numbers only</small>
	<?php
}
function pm_pdlp_del_opt_settings_field_callback($args) {
	$pm_pdlp_opts = $args[0];
	$del_opt_settings = ( empty( $pm_pdlp_opts['del_opt_settings'] ) ) ? '' : $pm_pdlp_opts['del_opt_settings'];
	?>
	<input type="checkbox" name="pm_pdlp_options[del_opt_settings]" value="1" id="del_opt_settings" <?php checked( $del_opt_settings, '1', true ); ?>><br>
	<small>Deletes all download pages and associated data. Only check if you are sure you will not use these download pages ever again.</small>
	<?php
}
function pm_pdlp_ambrdomain_field_callback($args) {
	$pm_pdlp_opts = $args[0];

	$ambrdomain = ( empty( $pm_pdlp_opts['ambrdomain'] ) ) ? '' : $pm_pdlp_opts['ambrdomain'];
	?>
	<input type="text" name="pm_pdlp_options[ambrdomain]" value="<?php echo $ambrdomain; ?>" id="ambrdomain" class="regular-text">
	<?php
}
function pm_pdlp_ambrfolder_field_callback($args) {
	$pm_pdlp_opts = $args[0];
	$ambrfolder = ( empty( $pm_pdlp_opts['ambrfolder'] ) ) ? '' : $pm_pdlp_opts['ambrfolder'];
	?>
	<input type="text" name="pm_pdlp_options[ambrfolder]" value="<?php echo $ambrfolder; ?>" id="ambrfolder" class="regular-text">
	<?php
}
function pm_pdlp_thanks_field_callback($args) {
	$pm_pdlp_opts = $args[0];
	$thanks = ( empty( $pm_pdlp_opts['thanks'] ) ) ? '' : stripslashes( $pm_pdlp_opts['thanks'] ) ;
	$settings = array(
		'textarea_name' => 'pm_pdlp_options[thanks]',
		'textarea_rows' => 5
		);
	wp_editor( $thanks, 'thanks', $settings );
}
function pm_pdlp_crosssell_field_callback($args) {
	$pm_pdlp_opts = $args[0];
	$crosssell = ( empty( $pm_pdlp_opts['crosssell'] ) ) ? '' : stripslashes( $pm_pdlp_opts['crosssell'] );
	$settings = array(
		'textarea_name' => 'pm_pdlp_options[crosssell]',
		'textarea_rows' => 5
		);
	wp_editor( $crosssell, 'crosssell', $settings );
}
// Validate options
function pm_pdlp_validate_options( $input ) {
	global $pm_pdlp_opts;
	$valid_input = $pm_pdlp_opts;

	// Determine which tab is submitted
	$general = ( !empty( $input['submit-general'] ) ? true : false );
	$amember = ( !empty( $input['submit-amember'] ) ? true : false );
	$messages = ( !empty( $input['submit-messages'] ) ? true : false );

	if( $general ) {
		// validate only those settings
		$valid_input['productslug'] = ( isset( $input['productslug'] ) ) ? sanitize_text_field( $input['productslug'] ) : '' ;
		$valid_input['amzaccess'] = ( isset( $input['amzaccess'] ) ) ? sanitize_text_field( $input['amzaccess'] ) : '' ;
		$valid_input['amzsecret'] = ( isset( $input['amzsecret'] ) ) ? sanitize_text_field( $input['amzsecret'] ) : '' ;
		$valid_input['defbucket'] = ( isset( $input['defbucket'] ) ) ? sanitize_text_field( $input['defbucket'] ) : '' ;
		$valid_input['bucketfolder'] = ( isset( $input['bucketfolder'] ) ) ? sanitize_text_field( $input['bucketfolder'] ) : '' ;
		$valid_input['pu_bucketfolder'] = ( isset( $input['pu_bucketfolder'] ) ) ? sanitize_text_field( $input['pu_bucketfolder'] ) : '' ;
		$valid_input['defexpiry'] = ( isset( $input['defexpiry'] ) && is_numeric( $input['defexpiry'] ) ) ? $input['defexpiry'] : '' ;
		$valid_input['del_opt_settings'] = ( isset( $input['del_opt_settings'] ) && $input['del_opt_settings'] == '1' ) ? '1' : '' ;

		// reuse ambr settings
		$valid_input['ambrdomain'] = $pm_pdlp_opts['ambrdomain'];
		$valid_input['ambrfolder'] = $pm_pdlp_opts['ambrfolder'];
		$valid_input['thanks'] = $pm_pdlp_opts['thanks'];
		$valid_input['crosssell'] = $pm_pdlp_opts['crosssell'];
	} elseif ( $amember ) {
		// reuse ambr settings
		$valid_input['productslug'] = $pm_pdlp_opts['productslug'];
		$valid_input['amzaccess'] = $pm_pdlp_opts['amzaccess'];
		$valid_input['amzsecret'] = $pm_pdlp_opts['amzsecret'];
		$valid_input['defbucket'] = $pm_pdlp_opts['defbucket'];
		$valid_input['bucketfolder'] = $pm_pdlp_opts['bucketfolder'];
		$valid_input['pu_bucketfolder'] = $pm_pdlp_opts['pu_bucketfolder'];
		$valid_input['defexpiry'] = $pm_pdlp_opts['defexpiry'];
		$valid_input['del_opt_settings'] = $pm_pdlp_opts['del_opt_settings'];
		$valid_input['thanks'] = $pm_pdlp_opts['thanks'];
		$valid_input['crosssell'] = $pm_pdlp_opts['crosssell'];

		// validate only those settings
		$valid_input['ambrdomain'] = ( isset( $input['ambrdomain'] ) ) ? sanitize_text_field( $input['ambrdomain'] ) : '' ;
		$valid_input['ambrfolder'] = ( isset( $input['ambrfolder'] ) ) ? sanitize_text_field( $input['ambrfolder'] ) : '' ;
	} elseif ( $messages ) {
		// reuse other settings
		$valid_input['productslug'] = $pm_pdlp_opts['productslug'];
		$valid_input['amzaccess'] = $pm_pdlp_opts['amzaccess'];
		$valid_input['amzsecret'] = $pm_pdlp_opts['amzsecret'];
		$valid_input['defbucket'] = $pm_pdlp_opts['defbucket'];
		$valid_input['bucketfolder'] = $pm_pdlp_opts['bucketfolder'];
		$valid_input['pu_bucketfolder'] = $pm_pdlp_opts['pu_bucketfolder'];
		$valid_input['defexpiry'] = $pm_pdlp_opts['defexpiry'];
		$valid_input['del_opt_settings'] = $pm_pdlp_opts['del_opt_settings'];
		$valid_input['ambrdomain'] = $pm_pdlp_opts['ambrdomain'];
		$valid_input['ambrfolder'] = $pm_pdlp_opts['ambrfolder'];

		// validate only these settings
		$valid_input['thanks'] = ( isset( $input['thanks'] ) ) ? wp_kses_post( $input['thanks'] ) : '' ;
		$valid_input['crosssell'] = ( isset( $input['crosssell'] ) ) ? wp_kses_post( $input['crosssell'] ) : '' ;
	}
	return $valid_input;
	/*update_option( 'pm_pdlp_options', $valid_input );
	print_r($valid_input);
	echo '<hr>';
	print_r($input);
	exit();*/
}
?>