<?php 
add_action( 'init', 'pm_pdlp_incl_amember' );
function pm_pdlp_incl_amember() {
	include_once('/Users/lynette/Documents/Websites/www.pluginmill.dev/account/library/Am/Lite.php');
	//include_once('/home/plugawes/public_html/account/library/Am/Lite.php');
}

ini_set('display_errors', true);  // for debug time

// Get files
function pm_pdlp_getAmbrFiles($post_id) {
$di  = Am_Di::getInstance(); // get a shortcut
	//global $di;
	$amemberFile = get_post_meta( $post_id, 'pm_pdlp_file', true );
	$pm_pdlp_file = ( empty($amemberFile) ) ? '0' : $amemberFile ;

	// Query aMember
	$files = $di->db->select("SELECT * FROM 4t2ambr_file ORDER BY file_id DESC");
	
	//print_r($files); // Check if results are correct
	// Echo out all products
	echo '<select name="pm_pdlp_file" '.selected( $pm_pdlp_file, '0', true ).'><option value="0">None</option>';
	foreach ($files as $file) {
		?>
		<option value="<?php echo sanitize_text_field( $file['file_id'] ); ?>" <?php selected( $pm_pdlp_file, $file['file_id'], true ); ?>><?php echo sanitize_text_field( $file['title'] ); ?></option>
		<?php
	}
	echo '</select>';
}

// Get products
function pm_pdlp_getAmbrProducts($post_id, $fieldname) {
	//$di  = Am_Di::getInstance(); // get a shortcut
	$di = Am_Lite::getInstance()->getProducts();
	$amemberPid = get_post_meta( $post_id, $fieldname, true );

	// Display products
	echo '<select class="get_'.$fieldname.'">';
	echo '<option value="0">None</option>';
	foreach ($di as $product_id => $product) {
		?>
		<option value="<?php echo sanitize_text_field( $product_id ); ?>"><?php echo sanitize_text_field( $product ); ?></option>
		<?php
	}
	echo '</select>';
	echo "<p id='show_".$fieldname."'>";
	if( !empty( $amemberPid ) ) {
		//print_r($amemberPid);
		foreach ($amemberPid as $k => $v) {
			$product_name = pm_pdlp_findAmbrProductTitle( $v, $di );
			echo '<span class="selected-items">'.$product_name.' ('.$v.')<input type="hidden" name="'.$fieldname.'[]" value="'.$v.'" class="item-selected"><span id="product-'.$v.'" class="deleteItem">X</span></span>';
		}
	}
	echo "</p>";
}

// Get saved product list only
function pm_pdlp_getAmbrProductList($post_id, $fieldname) {
	$di = Am_Lite::getInstance()->getProducts();
	$amemberPid = get_post_meta( $post_id, $fieldname, true );
		//print_r($amemberPid);

	// Display products
	echo "<p id='ambr-product-ids'>";
	if( !empty( $amemberPid ) ) {
		foreach ($amemberPid as $k => $v) {
			$product_name = pm_pdlp_findAmbrProductTitle( $v, $di );
			echo $product_name.' ('.$v.'), ';
		}
	}
	echo "</p>";
}

// aMember product name lookup
function pm_pdlp_findAmbrProductTitle($id, $array) {
	foreach ($array as $key => $value) {
		if( $key == $id ) {
			return $value;
		}
	}
}