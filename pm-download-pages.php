<?
/*
Plugin Name: Profitable Download Pages
Plugin URI: http://PluginMill.com
Description: Quick setup easily monetized download pages
Version: 1.2.2
Author: Lynette Chandler
Author URI: http://techbasedmarketing.com
Stable tag: 1.2.2
*/
/*
 * TODO:
 * _ remove files/folders after download? http://stackoverflow.com/a/8965853
 */
// Define constants
if(!defined('__DIR__')) {
    define( 'PM_PDLP_DIR', plugin_dir_url( __FILE__) );
    define( 'PM_PDLP_PATH', dirname( __FILE__) );
} else {
	define( 'PM_PDLP_DIR', WP_PLUGIN_URL.'/'.basename(__DIR__) );
	define( 'PM_PDLP_PATH', WP_PLUGIN_DIR.'/'.basename(__DIR__) );
}
define( 'PM_PDLP_WP_DIR', get_bloginfo('wpurl') );
define( 'PM_PDLP_PLUGIN_NAME', 'Profitable Download Pages' );
define( 'PM_PDLP_VERSION', '1.2.2' );
$pm_pdlp_opts = get_option( 'pm_pdlp_options' );

register_activation_hook( __FILE__, 'pm_pdlp_activate' );
function pm_pdlp_activate() {
global $pm_snappro_opts;

$ambrdomain = ( !empty( $pm_snappro_opts ) ) ? $pm_snappro_opts['ambrdomain'] : '' ;
$ambrfolder = ( !empty( $pm_snappro_opts ) ) ? $pm_snappro_opts['ambrfolder'] : '' ;

 $pm_pdlp_options = array(  
  'productslug' =>  'pm_pdlp',
  'ambrdomain' => $ambrdomain,
  'ambrfolder' => $ambrfolder,
  'del_opt_settings' =>  0,
  );
  if( !get_option('pm_pdlp_options') ){
	update_option('pm_pdlp_options', $pm_pdlp_options );
  }

}

include_once('modules/settings-api-tabbed.php');
include_once('modules/cpt.php');
include_once('modules/meta_box.php');
include_once('modules/amemberFuncs.php');
include_once('modules/functions.php');
include_once('modules/awss3.php');
include_once('modules/scripts.php');