<?php
// If uninstall not called from WordPress, exit
if( !defined( 'WP_UNINSTALL_PLUGIN' ) )
	exit ();
global $tbp_changeme_opts;

// Execute only when user chooses to
if ( $tbp_changeme_opts['del_opt_settings'] == '1' ) {
	// Delete option from options table
	delete_option( 'tbp_changeme_options' );
}

// Function to remove any additional options and custom tables

?>